#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <Windows.h>
#include <intrin.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "assert.h"

#include "raytracer.h"

#define ArrayCount(Arr) (sizeof(Arr) / sizeof(Arr[0]))

#include "ray_threading.cpp"

timer GlobalTimer = {};

// NOTE(Cristian): File size limit is basically hardcoded to ~4GB
function u32
GetImageSize(image_data *Image)
{
    u32 Result = Image->Width * Image->Height * sizeof(pixel_color);
    return(Result);
}

function image_data
CreateImage(u32 Width, u32 Height)
{
    image_data Result = {};
    Result.Width = Width;
    Result.Height = Height;
    
    u32 OutputImageSize = GetImageSize(&Result);
    
    bitmap_header Header = {};
    Header.Type = 0x4d42;
    Header.FileSize = sizeof(Header) * OutputImageSize;
    Header.OffsetBytes = sizeof(Header);
    
    Header.StructureSize = sizeof(Header) - OffsetOf(bitmap_header, StructureSize);
    Header.Width = Width;
    Header.Height = Height;
    Header.Planes = 1;
    Header.BitsPerPixel = sizeof(pixel_color) * 8;
    Header.Compression = 0;
    Header.SizeImage = OutputImageSize;
    Header.HorizontalPixelsPerMeter = 0;
    Header.VerticalPixelsPerMeter = 0;
    Header.ColorIndicesUsed = 0;
    Header.ColorIndicesImportant = 0;
    
    Result.Header = Header;
    Result.Data = (pixel_color *)malloc(OutputImageSize);
    Result.PixelCount = Width * Height;
    
    return(Result);
}

function void
WriteImageToFile(image_data *Image, pixel_data Data, char *Name)
{   
    for(u32 PixelIndex = 0;
        PixelIndex < Data.Count;
        ++PixelIndex)
    {
        vec4 PixelColor = Data.Data[PixelIndex];

        // DITHERING
        // http://eastfarthing.com/blog/2015-12-19-color/
        f32 DitherB = RandomF32() - 0.5f;
        f32 DitherG = RandomF32() - 0.5f;
        f32 DitherR = RandomF32() - 0.5f;
        Image->Data[PixelIndex].b = (u8)Clamp(PixelColor.b * 256.0f + DitherB, 0.0f, 255.0f);
        Image->Data[PixelIndex].g = (u8)Clamp(PixelColor.g * 256.0f + DitherG, 0.0f, 255.0f);
        Image->Data[PixelIndex].r = (u8)Clamp(PixelColor.r * 256.0f + DitherR, 0.0f, 255.0f);
        Image->Data[PixelIndex].a = 255;
    }

    FILE *OutputBitmapHandle = fopen(Name, "wb");
    if(OutputBitmapHandle)
    {
        fwrite(&Image->Header, sizeof(Image->Header), 1, OutputBitmapHandle);
        fwrite(Image->Data, GetImageSize(Image), 1, OutputBitmapHandle);
        fclose(OutputBitmapHandle);
    }
}

function f32
AttenuateNoCusp(f32 DistanceToLight, f32 LightRadius, f32 MaxIntensity, f32 FallOff)
{
    f32 Result = 0.0;
    f32 NormalizedDistance = DistanceToLight / LightRadius;
	if(NormalizedDistance < 1.0)
	{
        f32 SquaredNormalizedDistance = Square(NormalizedDistance);
		Result = MaxIntensity * Square(1.0f - SquaredNormalizedDistance) / (1.0f + FallOff * SquaredNormalizedDistance);
	}
    
	return(Result);
}

function hit_info
TraceRayInScene(vec4 RayOrigin, vec4 RayDirection, f32 MaxDistance, scene_storage *SceneStorage)
{
    f32 SmallestDistanceAlongRay = F32Max;
    
    hit_info HitInfo = {};
    
    // NOTE(Cristian): Normally the magnitude of A should be 1.0f after computing a dot product with itself,
    // but the ray direction vector might not be normalized.
    f32 A = DotSelf(RayDirection);
    
    for(u32 SphereIndex = 0;
        SphereIndex < SceneStorage->SpheresCount;
        ++SphereIndex)
    {
        sphere *CurrentSphere = &SceneStorage->Spheres[SphereIndex];
        
        vec4 SphereCenterToRayOrigin = RayOrigin - CurrentSphere->Center;
        
        f32 B = 2.0f * Dot(RayDirection, SphereCenterToRayOrigin);
        f32 C = DotSelf(SphereCenterToRayOrigin) - Square(CurrentSphere->Radius);
        
        f32 PIntersectRatio1, PIntersectRatio2;
        SolveQuadratic(A, B, C, &PIntersectRatio1, &PIntersectRatio2);
        
        f32 PIntersectRatio = PIntersectRatio2;
        if(PIntersectRatio1 > 0.0f)
        {
            PIntersectRatio = PIntersectRatio1;
        }
        
        if((PIntersectRatio > 0.0f) && (PIntersectRatio < SmallestDistanceAlongRay))
        {
            SmallestDistanceAlongRay = PIntersectRatio;
            
            HitInfo.EntityIndex = SphereIndex;
            HitInfo.EntityType = EntityType_Sphere;
            HitInfo.MaterialIndex = CurrentSphere->MaterialIndex;
        }
    }
    
    for(u32 PlaneIndex = 0;
        PlaneIndex < SceneStorage->PlanesCount;
        ++PlaneIndex)
    {
        plane *CurrentPlane = &SceneStorage->Planes[PlaneIndex];
        
        f32 Denominator = Dot(CurrentPlane->Normal, RayDirection);
        if(fabs(Denominator) > 0.0001f)
        {
            f32 PIntersectionRatio = Dot((CurrentPlane->Origin - RayOrigin), CurrentPlane->Normal) / Denominator;
            if(PIntersectionRatio >= 0.0f)
            {
                if(PIntersectionRatio < SmallestDistanceAlongRay)
                {
                    SmallestDistanceAlongRay = PIntersectionRatio;
                    
                    HitInfo.EntityIndex = PlaneIndex;
                    HitInfo.EntityType = EntityType_Plane;
                    HitInfo.MaterialIndex = CurrentPlane->MaterialIndex;
                }
            }
        }
    }
    
    if(HitInfo.EntityType != EntityType_Unknown)
    {
        HitInfo.HitPosition = RayOrigin + SmallestDistanceAlongRay * RayDirection;
        if(VectorLength(HitInfo.HitPosition - V4(0.0f)) <= MaxDistance)
        {
            HitInfo.MadeContact = true;

            switch(HitInfo.EntityType)
            {
                case EntityType_Plane:
                {
                    HitInfo.HitSurfaceNormal = SceneStorage->Planes[HitInfo.EntityIndex].Normal;
                } break;
                
                case EntityType_Sphere:
                {
                    HitInfo.HitSurfaceNormal = Normalize(HitInfo.HitPosition - SceneStorage->Spheres[HitInfo.EntityIndex].Center);
                } break;
            }
        }
        else
        {
            HitInfo = {};
        }
    }
    
    return(HitInfo);
}

function vec3
GetLinearizedValue(vec3 Data, f32 Gamma)
{
    vec3 LinearizedResult = V3(powf(Data.x, Gamma),
                               powf(Data.y, Gamma),
                               powf(Data.z, Gamma));
    return(LinearizedResult);
}

// function f32
// NDF(f32 Alpha, vec3 Normal, vec3 HalfVector)
// {
//     f32 AlphaSquared = Square(Alpha);
//     f32 Numerator = AlphaSquared;
    
//     f32 NDotH = Max(Dot(Normal, HalfVector), 0.0f);
//     f32 Denominator = (f32)(PI * Square(Square(NDotH) * (AlphaSquared - 1) + 1));
//     Denominator = Max(Denominator, 0.0001f);
    
//     return(Numerator / Denominator);
// }

// // Smith's Schlick GGX approximation
// function f32
// G1(f32 K, vec3 Normal, vec3 X)
// {
//     f32 Numerator = Max(Dot(Normal, X), 0.0f);
//     f32 Denominator = Max(Dot(Normal, X), 0.0f) * (1.0f - K) + K;
//     Denominator = Max(Denominator, 0.0001f);
    
//     return(Numerator / Denominator);
// }

// // Smith model (k = alpha / 2 reformulation)
// function f32
// GeometryFunction(f32 Alpha, vec3 Normal, vec3 ViewVector, vec3 LightVector)
// {
//     f32 K = Alpha * 0.5f;
//     return(G1(K, Normal, ViewVector) * G1(K, Normal, LightVector));
// }

// function vec3
// FresnelFunction(vec3 F0, f32 CosTheta)
// {
//     vec3 Result = F0 + (V3(1.0f) - F0) * powf(Clamp(1.0f - CosTheta, 0.0f, 1.0f), 5.0f);
//     return(Result);
// }

// function vec3
// PBR(material *SurfaceMaterial, f32 Gamma,
//     vec3 ViewVector, vec3 Normal, vec3 HalfVector, 
//     vec3 LightVector, vec3 LightVectorDirection, point_light *Pointlight)
// {
//     vec3 LinearizedAlbedo = GetLinearizedValue(SurfaceMaterial->Albedo, Gamma);
//     //vec3 LinearizedAlbedo = SurfaceMaterial->Albedo;
    
//     f32 Alpha = Square(SurfaceMaterial->Roughness);
//     vec3 F0 = Lerp(V3(0.04f), LinearizedAlbedo, SurfaceMaterial->Metalness);
    
//     vec3 Fresnel = FresnelFunction(F0, Max(Dot(HalfVector, ViewVector), 0.0f));
//     vec3 Ks = Fresnel;
//     vec3 Kd = V3(1.0f) - Ks;
    
//     // NOTE(Cristian): Ignore diffuse reflections if surface is metallic
//     Kd *= (1.0f - SurfaceMaterial->Metalness);
    
//     vec3 Lambert = LinearizedAlbedo / (f32)PI;
//     vec3 Numerator = 
//         NDF(Alpha, Normal, HalfVector) * 
//         Fresnel * 
//         GeometryFunction(Alpha, Normal, ViewVector, LightVectorDirection);
    
//     f32 CosTheta = Max(Dot(Normal, LightVectorDirection), 0.0f);
//     f32 LightAttenuationFactor = AttenuateNoCusp(VectorLength(LightVector), 
//                                                  Pointlight->Radius, 
//                                                  Pointlight->Intensity, 
//                                                  Pointlight->FallOff);
//     //f32 LightAttenuationFactor = 1.0f / VectorLength(LightVector);
    
//     vec3 Radiance = Pointlight->Color * LightAttenuationFactor;
    
//     f32 Denominator = 4.0f * Max(Dot(Normal, ViewVector), 0.0f) * CosTheta;
//     Denominator = Max(Denominator, 0.0001f);
    
//     vec3 CookTorrance = Numerator / Denominator;
//     vec3 BRDF = Kd * Lambert + CookTorrance;
//     vec3 LightOutput = BRDF * Radiance * CosTheta;
//     return(LightOutput);
// }

// function vec3
// ComputeLightContribution(hit_info *HitInfo, scene_storage *SceneStorage)
// {
//     vec3 FinalLightContribution = {};
//     for(u32 PointlightIndex = 0;
//         PointlightIndex < SceneStorage->PointlightsCount;
//         ++PointlightIndex)
//     {
//         point_light *CurrentPointlight = &SceneStorage->Pointlights[PointlightIndex];
        
//         vec3 HitPositionToLight = CurrentPointlight->Position - HitInfo->HitPosition;
//         vec3 HitPositionToLightDirection = Normalize(HitPositionToLight);
        
//         f32 AmbientContributionFactor = 0.05f;
//         vec3 AmbientContribution = CurrentPointlight->Color * AmbientContributionFactor;
        
//         f32 DiffuseContributionFactor = Dot(HitInfo->HitSurfaceNormal, HitPositionToLightDirection);
//         DiffuseContributionFactor = Max(DiffuseContributionFactor, 0.0f);
        
//         vec3 DiffuseContribution = CurrentPointlight->Color * DiffuseContributionFactor * CurrentPointlight->Intensity;
//         f32 LightAttenuationFactor = AttenuateNoCusp(VectorLength(HitPositionToLight), 
//                                                      CurrentPointlight->Radius, 
//                                                      CurrentPointlight->Intensity, 
//                                                      CurrentPointlight->FallOff);
        
//         f32 RayOriginOffset = 0.005f;
//         hit_info ShadowRayInfo = TraceRayInScene(HitInfo->HitPosition + RayOriginOffset * HitPositionToLightDirection,
//                                                  HitPositionToLightDirection, VectorLength(HitInfo->HitPosition - CurrentPointlight->Position),
//                                                  SceneStorage);
//         u32 ShadowContribution = ShadowRayInfo.MaterialIndex ? 1 : 0;
        
//         FinalLightContribution += ((DiffuseContribution * LightAttenuationFactor) * (1.0f - ShadowContribution) + AmbientContribution);
//     }
    
//     return(FinalLightContribution);
// }

function void
Blur3x3(image_data *Image)
{
    for(u32 Y = 0;
        Y < Image->Height;
        ++Y)
    {
        for(u32 X = 0;
            X< Image->Width;
            ++X)
        {
            f32 RAccum = 0;
            f32 GAccum = 0;
            f32 BAccum = 0;
            
            s32 CenterPixelCoords = Y * Image->Width + X;
            for(s32 WindowY = -1;
                WindowY <= 1;
                ++WindowY)
            {
                for(s32 WindowX = -1;
                    WindowX <= 1;
                    ++WindowX)
                {
                    s32 CurrentPixelIndex = CenterPixelCoords + WindowX * Image->Width + WindowY;
                    if(CurrentPixelIndex < 0)
                    {
                        CurrentPixelIndex = 0;
                    }
                    else if(CurrentPixelIndex >= (s32)(Image->Width * Image->Height))
                    {
                        CurrentPixelIndex = (Image->Width - 1) * (Image->Height - 1);
                    }
                    
                    pixel_color *PixelColor = &Image->Data[CurrentPixelIndex];
                    RAccum += (PixelColor->r + 0.5f) / 256.0f;
                    GAccum += (PixelColor->g + 0.5f) / 256.0f;
                    BAccum += (PixelColor->b + 0.5f) / 256.0f;
                }
            }
            
            RAccum /= 9.0f;
            GAccum /= 9.0f;
            BAccum /= 9.0f;
            
            /*f32 DitherB = RandomF32() - 0.5f;
            f32 DitherG = RandomF32() - 0.5f;
            f32 DitherR = RandomF32() - 0.5f;*/
            /*Color->b = (u8)(BAccum * 256.0f + DitherB);
            Color->g = (u8)(GAccum * 256.0f + DitherG);
            Color->r = (u8)(RAccum * 256.0f + DitherR);*/
            pixel_color *Color = &Image->Data[CenterPixelCoords];
            Color->b = (u8)(Clamp(BAccum * 256.0f, 0.0f, 255.99f));
            Color->g = (u8)(Clamp(GAccum * 256.0f, 0.0f, 255.99f));
            Color->r = (u8)(Clamp(RAccum * 256.0f, 0.0f, 255.99f));
        }
    }
}

function void
GaussianBlur(image_data *Image, pixel_data PixelData, b32 HorizontalBlur, f32 *WeightsData, u32 WeightsCount)
{
    u32 PixelIndex = 0;
    for(s32 Y = 0;
        Y < (s32)Image->Height;
        ++Y)
    {
        for(s32 X = 0;
            X < (s32)Image->Width;
            ++X)
        {
            vec4 CenterPixelColor = PixelData.Data[PixelIndex] * WeightsData[0];

            for(u32 WeightIndex = 1;
                WeightIndex < WeightsCount;
                ++WeightIndex)
            {   
                vec4 PreviousPixelColor = V4(0.0f);
                vec4 NextPixelColor = V4(0.0f);
                if(HorizontalBlur)
                {
                    if((s32)(X - WeightIndex) >= 0)
                    {
                        PreviousPixelColor = PixelData.Data[PixelIndex - WeightIndex] * WeightsData[WeightIndex];
                    }

                    if((s32)(X + WeightIndex) <= (Image->Width - 1))
                    {
                        NextPixelColor = PixelData.Data[PixelIndex + WeightIndex] * WeightsData[WeightIndex];
                    }
                }
                else
                {
                    // Vertical blur
                    if((s32)(Y - WeightIndex) >= 0)
                    {
                        PreviousPixelColor = PixelData.Data[PixelIndex - Image->Width * WeightIndex] * WeightsData[WeightIndex];
                    }

                    if((s32)(Y + WeightIndex) <= (Image->Height - 1))
                    {
                        NextPixelColor = PixelData.Data[PixelIndex + Image->Width * WeightIndex] * WeightsData[WeightIndex];
                    }
                }

                CenterPixelColor += PreviousPixelColor + NextPixelColor;
            }

            PixelData.Data[PixelIndex] = CenterPixelColor;

            PixelIndex++;
        }
    }
}

function material *
PushMaterial(scene_storage *Storage, vec4 Albedo = V4(0.0f))
{
    assert(Storage->MaterialsCount < ArrayCount(Storage->Materials));
    
    material *Result = &Storage->Materials[Storage->MaterialsCount++];
    Result->Albedo = Albedo;
    Result->Roughness = 0.0f;
    Result->Metalness = 0.3f;
    Result->Emissive = V4(0.0f);
    Result->EmissiveStrength = 0.0f;
    return(Result);
}

function void
InitializeScene(scene_storage *SceneStorage)
{
    SceneStorage->MaterialsCount = 0;
    
    material *SkyMaterial = PushMaterial(SceneStorage, V4(V3(0.3f, 0.5f, 0.9f)));
    /*SkyMaterial->EmissiveStrength = 0.1f;
    SkyMaterial->Albedo = SkyMaterial->Albedo * SkyMaterial->EmissiveStrength;
    SkyMaterial->Emissive = SkyMaterial->Albedo;*/
    
    material *WhiteSphereMaterial1 = PushMaterial(SceneStorage, V4(V3(1.0f), 0.0f));
    WhiteSphereMaterial1->Roughness = 1.0f;
    material *WhiteSphereMaterial2 = PushMaterial(SceneStorage, V4(V3(1.0f), 0.0f));
    WhiteSphereMaterial2->Roughness = 0.8f;
    material *WhiteSphereMaterial3 = PushMaterial(SceneStorage, V4(V3(1.0f), 0.0f));
    WhiteSphereMaterial3->Roughness = 0.6f;
    material *WhiteSphereMaterial4 = PushMaterial(SceneStorage, V4(V3(1.0f), 0.0f));
    WhiteSphereMaterial4->Roughness = 0.4f;
    
    material *BlueSphereMaterial = PushMaterial(SceneStorage, V4(V3(0.01f, 0.8f, 1.0f)));
    //BlueSphereMaterial->Roughness = 1.0f;
    /*BlueSphereMaterial->EmissiveStrength = 2.0f;
    BlueSphereMaterial->Albedo = BlueSphereMaterial->Albedo * BlueSphereMaterial->EmissiveStrength;
    BlueSphereMaterial->Emissive = BlueSphereMaterial->Albedo;*/
    
    material *RedSphereMaterial = PushMaterial(SceneStorage, V4(V3(1.0f, 0.01f, 0.01f)));
    /*RedSphereMaterial->EmissiveStrength = 20.0f;
    RedSphereMaterial->Albedo = RedSphereMaterial->Albedo * RedSphereMaterial->EmissiveStrength;
    RedSphereMaterial->Emissive = V3(1.0f, 0.01f, 0.01f);*/
    
    material *PlaneMaterial = PushMaterial(SceneStorage, V4(1.0f));
    
    material *WhiteSphereMaterial = PushMaterial(SceneStorage);
    WhiteSphereMaterial->EmissiveStrength = 2.0f;
    WhiteSphereMaterial->Emissive = V4(1.0f);
    WhiteSphereMaterial->Albedo = V4(1.0f);
    
    material *EmissivePlaneMaterial = PushMaterial(SceneStorage);
    EmissivePlaneMaterial->EmissiveStrength = 1.0f;
    //EmissivePlaneMaterial->Albedo = EmissivePlaneMaterial->Albedo * EmissivePlaneMaterial->EmissiveStrength;
    EmissivePlaneMaterial->Emissive = V4(1.0f);
    
    material *RedPlaneMaterial = PushMaterial(SceneStorage, V4(V3(0.8f, 0.01f, 0.01f)));
    
    material *GreenPlaneMaterial = PushMaterial(SceneStorage, V4(V3(0.01f, 0.8f, 0.01f)));
    
    SceneStorage->Spheres[0].Center = V4(V3(0.0f, 4.5f, 0.0f));
    SceneStorage->Spheres[0].Radius = 1.0f;
    SceneStorage->Spheres[0].MaterialIndex = 8;
    
    SceneStorage->Spheres[1].Center = V4(V3(-3.7f, 2.0f, 0.0f));
    SceneStorage->Spheres[1].Radius = 1.0f;
    SceneStorage->Spheres[1].MaterialIndex = 1;
    
    SceneStorage->Spheres[2].Center = V4(V3(-1.2f, 2.0f, 0.0f));
    SceneStorage->Spheres[2].Radius = 1.0f;
    SceneStorage->Spheres[2].MaterialIndex = 2;
    
    SceneStorage->Spheres[3].Center = V4(V3(1.2f, 2.0f, 0.0f));
    SceneStorage->Spheres[3].Radius = 1.0f;
    SceneStorage->Spheres[3].MaterialIndex = 3;
    
    SceneStorage->Spheres[4].Center = V4(V3(3.7f, 2.0f, 0.0f));
    SceneStorage->Spheres[4].Radius = 1.0f;
    SceneStorage->Spheres[4].MaterialIndex = 4;
    SceneStorage->SpheresCount = 5;
    
    // FLOOR
    SceneStorage->Planes[0].Origin = V4(0.0f);
    SceneStorage->Planes[0].Normal = V4(V3(0.0f, 1.0f, 0.0f));
    SceneStorage->Planes[0].MaterialIndex = 7;
    
    // LEFT WALL
    SceneStorage->Planes[1].Origin = V4(V3(-5.5f, 0.0f, 0.0f));
    SceneStorage->Planes[1].Normal = V4(V3(1.0f, 0.0f, 0.0f));
    SceneStorage->Planes[1].MaterialIndex = 10;
    
    // RIGHT WALL
    SceneStorage->Planes[2].Origin = V4(V3(5.5f, 0.0f, 0.0f));
    SceneStorage->Planes[2].Normal = V4(V3(-1.0f, 0.0f, 0.0f));
    SceneStorage->Planes[2].MaterialIndex = 11;
    
    // BACK WALL
    SceneStorage->Planes[3].Origin = V4(V3(0.0f, 0.0f, -3.2f));
    SceneStorage->Planes[3].Normal = V4(V3(0.0f, 0.0f, 1.0f));
    SceneStorage->Planes[3].MaterialIndex = 7;
    
    // CEILING
    SceneStorage->Planes[4].Origin = V4(V3(0.0f, 6.5f, 0.0f));
    SceneStorage->Planes[4].Normal = V4(V3(0.0f, -1.0f, 0.0f));
    SceneStorage->Planes[4].MaterialIndex = 7;
    SceneStorage->PlanesCount = 5;
    
    SceneStorage->Pointlights[0].Position = V4(V3(0.0f, 2.5f, 4.0f));
    SceneStorage->Pointlights[0].Color = V4(1.0f);
    SceneStorage->Pointlights[0].Radius = 20.0f;
    SceneStorage->Pointlights[0].FallOff = 15.0f;
    SceneStorage->Pointlights[0].Intensity = 2.5f;
    SceneStorage->PointlightsCount = 1;
    
    SceneStorage->Directionallights[0].Direction = V4(V3(0.0f, 100.0f, 20.0f));
    SceneStorage->Directionallights[0].Color = V4(1.0f);
    SceneStorage->DirectionallightsCount = 0;
}

function vec4
RandomDirection()
{
    vec4 Result = V4(RandomNormalDistribution(), 
                     RandomNormalDistribution(), 
                     RandomNormalDistribution(),
                     0.0f);
    return(Normalize(Result));
}

function vec4
RandomHemisphereDirection(vec4 Normal)
{
    vec4 Direction = RandomDirection();
    return(Direction * Sign(Dot(Normal, Direction)));
}

function b32
RayHitASurface(hit_info *Info)
{
    b32 Result = Info->MaterialIndex;
    return(Result);
}

function f32
GetLuminance(vec4 PixelColor)
{
    return(Dot(PixelColor, V4(V3(0.2126f, 0.7152f, 0.0722f))));
}

function f32
ExtractHighlights(pixel_data SourceData, pixel_data DestinationData, f32 Threshold = 1.0f)
{
    f32 HighestLuminance = 0.0f;

    for(u32 PixelIndex = 0;
        PixelIndex < SourceData.Count;
        ++PixelIndex)
    {
        vec4 CurrentPixelColor = SourceData.Data[PixelIndex];

        f32 PixelBrightness = GetLuminance(CurrentPixelColor);
        if(PixelBrightness >= Threshold)
        {
            DestinationData.Data[PixelIndex] = CurrentPixelColor;
        }

        if(PixelBrightness > HighestLuminance)
        {
            HighestLuminance = PixelBrightness;
        }
    }

    return(HighestLuminance);
}

// Collapses HDR values into LDR/SDR
function vec4
ApplyReinhardExtendedLuminance(vec4 PixelColor, f32 MaxWhiteLight = 0.001f)
{
    f32 OldLuminance = GetLuminance(PixelColor);
    f32 Numerator = OldLuminance * (1.0f + (OldLuminance / (Square(MaxWhiteLight))));
    f32 NewLuminance = Numerator / (1.0f + OldLuminance);
    return(PixelColor * (NewLuminance / OldLuminance));
}

function void
HDRToLDR(pixel_data Data, f32 Gamma, b32 ApplyTonemapping = false, f32 MaxWhitePoint = 0.0f)
{
    for(u32 PixelIndex = 0;
        PixelIndex < Data.Count;
        ++PixelIndex)
    {
        vec4 PixelColor = Data.Data[PixelIndex];

        if(ApplyTonemapping)
        {
            // Standard Reinhard tone mapping
            //PixelColor /= (PixelColor + V3(1.0f));

            // Extended Reinhard Luminance Tonemapping
            PixelColor = ApplyReinhardExtendedLuminance(PixelColor, MaxWhitePoint);
        }

        // Gamma correction
        PixelColor = V4(powf(PixelColor.r, 1.0f / Gamma),
                        powf(PixelColor.g, 1.0f / Gamma),
                        powf(PixelColor.b, 1.0f / Gamma),
                        0.0f);
        
        Data.Data[PixelIndex] = PixelColor;
    }
}

function void
ApplyGaussianBlurToImage(pixel_data SourceBloomData, pixel_data DestinationData)
{
    for(u32 PixelIndex = 0;
        PixelIndex < DestinationData.Count;
        ++PixelIndex)
    {
        DestinationData.Data[PixelIndex] += SourceBloomData.Data[PixelIndex];
    }
}


// image_data OutputImage = CreateImage(3840, 2160);
// image_data BloomImage = CreateImage(3840, 2160);

// image_data OutputImage = CreateImage(2560, 1440);
// image_data BloomImage = CreateImage(2560, 1440);

image_data OutputImage = CreateImage(1920, 1080);
image_data BloomImage = CreateImage(1920, 1080);

//image_data OutputImage = CreateImage(800, 600);
//image_data OutputImage = CreateImage(400, 300);

u32 volatile CurrentProgress = 0;
u32 volatile PixelsTraced = 0;
u32 volatile TotalPixelsToTrace = OutputImage.Width * OutputImage.Height;

ticket_mutex GlobalMutex = {};

JOB_QUEUE_ENTRY_CALLBACK(ProcessPixel)
{
    job_pixel_data *PixelData = (job_pixel_data *)Data;
    
    vec4 PixelColorAccumulator = V4(0.0f);

    timer ThreadTimer = {};
    StartTimer(&ThreadTimer);
       
    for(u32 RayIndex = 0;
        RayIndex < PixelData->NumberOfRays;
        ++RayIndex)
    {
        f32 DisplacementX = (RandomF32() - 0.5f) * PixelData->PixelInfo.HalfPixelWSize;
        f32 DisplacementY = (RandomF32() - 0.5f) * PixelData->PixelInfo.HalfPixelHSize;

        // NOTE(Cristian): ViewportCenter is in worldspace coordinates
        vec4 ViewportCenter = PixelData->ViewportCenter;
        ViewportCenter.x += (PixelData->ViewportX + DisplacementX) * PixelData->ViewportInfo.HalfViewportWidth;
        ViewportCenter.y += (PixelData->ViewportY + DisplacementY) * PixelData->ViewportInfo.HalfViewportHeight;
        
        vec4 CameraRay = Normalize(ViewportCenter - PixelData->CameraPos);
        
        vec4 RayOrigin = PixelData->CameraPos;
        vec4 RayDirection = CameraRay;
        
        vec4 IncomingLight = V4(0.0f);
        vec4 RayColor = V4(1.0f);
        for(u32 RayBounceIndex = 0;
            RayBounceIndex < PixelData->TotalBouncesPerPixel;
            ++RayBounceIndex)
        {
            hit_info HitData = TraceRayInScene(RayOrigin, RayDirection, PixelData->FarPlane, PixelData->WorldStorage);
            if(!HitData.MadeContact)
            {
                break;
            }

            material *Material = &PixelData->WorldStorage->Materials[HitData.MaterialIndex];
            
            vec4 EmittedLight = Material->Emissive * Material->EmissiveStrength;
            IncomingLight += EmittedLight * RayColor;

            RayColor *= V4(GetLinearizedValue(V3(Material->Albedo), PixelData->Gamma), 0.0f);

            if(RayColor == V4(0.0f))
            {
                IncomingLight -= EmittedLight * RayColor;
                break;
            }
            
            vec4 DiffuseDir = RandomHemisphereDirection(HitData.HitSurfaceNormal);
            vec4 SpecularDir = RayDirection - 2.0f * Dot(RayDirection, HitData.HitSurfaceNormal) * HitData.HitSurfaceNormal;
            RayDirection = Lerp(DiffuseDir, SpecularDir, Material->Roughness);
            RayOrigin = HitData.HitPosition + 0.0005f * RayDirection;
        }
        
        PixelColorAccumulator += (IncomingLight);
    }
    
    // Average the color values of all rays shot into a pixel
    PixelColorAccumulator /= (f32)PixelData->NumberOfRays;
    PixelData->OutputData.Data[PixelData->DataIndex] += (PixelColorAccumulator / (f32)PixelData->TotalFrames);

    StopTimer(&ThreadTimer);

    TicketMutexLock(&GlobalMutex);
    {
        PixelData->RaytracerStatistics->TotalPixelsTraced++;
        PixelData->RaytracerStatistics->AverageTimePerPixel_MS += GetTimeInMilliseconds(&ThreadTimer);

        u32 NewProgress = (PixelData->RaytracerStatistics->TotalPixelsTraced * 100) / TotalPixelsToTrace;
        if(NewProgress != CurrentProgress)
        {
            CurrentProgress = NewProgress;
            printf("-");
        }
    }
    TicketMutexUnlock(&GlobalMutex);
}

function void
PrintStatistics(raytracer_statistics Statistics)
{
    printf("Render resolution: %d x %d \n", OutputImage.Width, OutputImage.Height);
    printf("Total render time: %f seconds \n", Statistics.TotalRenderTime_S);
    printf("Total pixels traced: %d \n", Statistics.TotalPixelsTraced);
    printf("Average time per pixel: %f ms \n", Statistics.AverageTimePerPixel_MS);
}

s32 main()
{
    raytracer_statistics Statistics = {};

    const u32 ThreadsCount = 23;
    thread_info ThreadInfos[ThreadsCount];
    job_queue ProcessingQueue = {};
    MakeJobQueue(&ProcessingQueue, ThreadInfos, ThreadsCount);
    
    u32 NumberOfRays = 16;
    u32 TotalBouncesPerPixel = 4;
    u32 TotalFrames = 1;
    f32 Gamma = 2.2f;
    
    scene_storage *WorldStorage = (scene_storage *)malloc(sizeof(scene_storage));
    InitializeScene(WorldStorage);
    
    f32 NearPlane = 1.0f;
    f32 FarPlane = 1000.0f;
    
    viewport_info ViewportInfo = {};
    ViewportInfo.ViewportWidth = 1.0f;
    ViewportInfo.ViewportHeight = 1.0f;
    ViewportInfo.HalfViewportWidth = 0.5f * ViewportInfo.ViewportWidth;
    ViewportInfo.HalfViewportHeight = 0.5f * ViewportInfo.ViewportHeight;
    
    f32 AspectRatio = (f32)OutputImage.Width / (f32)OutputImage.Height;
    
    vec4 Target = V4(0.0f, 3.0f, -1.0f, 0.0f);
    
    vec4 CameraPos = V4(0.0f, 4.0f, 22.0f, 0.0f);
    vec4 CameraZ = Normalize(CameraPos - Target);
    vec4 CameraX = V4(Normalize(Cross(V3(0.0f, 1.0f, 0.0f), V3(CameraZ))), 0.0f);
    vec4 CameraY = V4(Normalize(Cross(V3(CameraZ), V3(CameraX))), 0.0f);
    
    vec4 ViewportCenter = CameraPos - NearPlane * CameraZ;
    
    pixel_info PixelInfo = {};
    PixelInfo.PixelWidthSize = 1.0f / OutputImage.Width;
    PixelInfo.PixelHeightSize = 1.0f / OutputImage.Height;
    PixelInfo.HalfPixelWSize =  PixelInfo.PixelWidthSize * 0.5f;
    PixelInfo.HalfPixelHSize =  PixelInfo.PixelHeightSize * 0.5f;

    u32 PixelDataSize = sizeof(vec4);
    u32 TotalPixelDataSize = OutputImage.Height * OutputImage.Width * PixelDataSize;

    pixel_data OutputData = {};
    OutputData.Data = (vec4 *)malloc(TotalPixelDataSize);
    OutputData.Count = OutputImage.Height * OutputImage.Width;

    pixel_data BloomOutputData = {};
    BloomOutputData.Data = (vec4 *)malloc(TotalPixelDataSize);
    BloomOutputData.Count = BloomImage.Height * BloomImage.Width;

    job_pixel_data *UnprocessedData = (job_pixel_data *)malloc(OutputImage.Width * OutputImage.Height * sizeof(job_pixel_data));

    if(OutputData.Data && BloomOutputData.Data && UnprocessedData)
    {
        StartTimer(&GlobalTimer);

        ZeroMemory(OutputData.Data, TotalPixelDataSize);
        ZeroMemory(BloomOutputData.Data, TotalPixelDataSize);
        
        printf("Progress:\n");
        printf("==================================================");
        printf("==================================================\n");
        
        for(u32 FrameIndex = 0;
            FrameIndex < TotalFrames;
            ++FrameIndex)
        {   
            u32 PixelIndex = 0;
            for(u32 Y = 0;
                Y < OutputImage.Height;
                ++Y)
            {
                f32 ViewportY = MapToRange((f32)Y, 0, (f32)OutputImage.Height, -ViewportInfo.HalfViewportHeight, ViewportInfo.HalfViewportHeight);
                
                for(u32 X = 0;
                    X < OutputImage.Width;
                    ++X)
                {
                    f32 ViewportX = MapToRange((f32)X, 0, (f32)OutputImage.Width, -ViewportInfo.HalfViewportWidth, ViewportInfo.HalfViewportWidth);
                    ViewportX *= AspectRatio;
                    
                    if(ProcessingQueue.AvailableEntriesCount == ArrayCount(ProcessingQueue.JobEntries))
                    {
                        while(ProcessingQueue.AvailableEntriesCount > ThreadsCount + 1)
                        {
                            DoNextAvailableJobEntry(&ProcessingQueue, 0);
                        }
                    }

                    job_pixel_data *CurrentUnprocessedData = &UnprocessedData[PixelIndex];
                    CurrentUnprocessedData->Gamma = Gamma;
                    CurrentUnprocessedData->NumberOfRays = NumberOfRays;
                    CurrentUnprocessedData->TotalBouncesPerPixel = TotalBouncesPerPixel;
                    CurrentUnprocessedData->TotalFrames = TotalFrames;
                    CurrentUnprocessedData->OutputData = OutputData;
                    CurrentUnprocessedData->PixelInfo = PixelInfo;
                    CurrentUnprocessedData->ViewportInfo = ViewportInfo;
                    CurrentUnprocessedData->FarPlane = FarPlane;
                    CurrentUnprocessedData->ViewportCenter = ViewportCenter;
                    CurrentUnprocessedData->CameraX = CameraX;
                    CurrentUnprocessedData->CameraY = CameraY;
                    CurrentUnprocessedData->CameraPos = CameraPos;
                    CurrentUnprocessedData->WorldStorage = WorldStorage;

                    CurrentUnprocessedData->DataIndex = PixelIndex++;
                    CurrentUnprocessedData->ViewportX = ViewportX;
                    CurrentUnprocessedData->ViewportY = ViewportY;

                    CurrentUnprocessedData->RaytracerStatistics = &Statistics;
                    
                    AddJobEntry(&ProcessingQueue, ProcessPixel, CurrentUnprocessedData, 0);
                }
            }
            
            CompleteAllWork(&ProcessingQueue, 0);
            
            printf(" [Finished frame %d] \n", FrameIndex + 1);
        }

        Statistics.AverageTimePerPixel_MS /= Statistics.TotalPixelsTraced;

        StopTimer(&GlobalTimer);
        Statistics.TotalRenderTime_S = GetTimeInSeconds(&GlobalTimer);

        StartTimer(&GlobalTimer);

        printf("Post processing");

        f32 CurrentTimeMS = GetCurrentTimeInMilliseconds(&GlobalTimer);

        f32 MaxWhitePoint = ExtractHighlights(OutputData, BloomOutputData, 1.1f);

        f32 Weights[5] = { 0.227027f, 0.1945946f, 0.1216216f, 0.054054f, 0.016216f };
        u32 TimesToBlur = 5;
        for(u32 Idx = 0;
            Idx < TimesToBlur;
            ++Idx)
        {
            f32 CurrentTime = GetCurrentTimeInMilliseconds(&GlobalTimer);
            if(CurrentTime >= 1000)
            {
                printf(".");
                ResetTimer(&GlobalTimer);
            }

            GaussianBlur(&BloomImage, BloomOutputData, false, Weights, ArrayCount(Weights));
            GaussianBlur(&BloomImage, BloomOutputData, true, Weights, ArrayCount(Weights));
        }
        printf(" 100%% \n\n");

        ApplyGaussianBlurToImage(BloomOutputData, OutputData);

        GaussianBlur(&OutputImage, OutputData, false, Weights, ArrayCount(Weights));
        GaussianBlur(&OutputImage, OutputData, true, Weights, ArrayCount(Weights));

        HDRToLDR(OutputData, Gamma, false, MaxWhitePoint);
        HDRToLDR(BloomOutputData, Gamma);
        
        WriteImageToFile(&OutputImage, OutputData, "ray.bmp");
        WriteImageToFile(&BloomImage, BloomOutputData, "bloom_data.bmp");

        PrintStatistics(Statistics);
    }
    else
    {
        printf("Memory allocation failed! \n");
    }
    
    return(0);
}