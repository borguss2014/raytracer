#pragma once

#include <cstdint>

#include "platform_shared.h"
#include "../application/application_math.h"
#include "../application/application_memory.h"
#include "../application/application_storage.h"

memory_arena DefaultArena = {};

platform_api Platform;

function byte_buffer
PushByteBuffer(u32 Size, memory_arena *Arena = &DefaultArena)
{
    byte_buffer Result = {};
    Result.Data = (u8 *)PushSlice(Arena, Size);
    Result.Capacity = Size;
    return(Result);
}

enum pixel_format
{
    PixelFormat_None = 0,

    PixelFormat_BGR8,
    PixelFormat_RGB32,
    PixelFormat_BGRA8,
    PixelFormat_RGBA32,
    PixelFormat_COUNT
};

struct pixel_format_info
{
    u32 Components;
    u32 ComponentByteSize;
};

function pixel_format_info
GetPixelFormatInfo(pixel_format Format)
{
    pixel_format_info Result = {};

    switch(Format)
    {
        case PixelFormat_RGBA32:
        {
            Result.Components = 4;
            Result.ComponentByteSize = sizeof(f32);
        } break;

        case PixelFormat_BGRA8:
        {
            Result.Components = 4;
            Result.ComponentByteSize = sizeof(u8);
        } break;

        case PixelFormat_RGB32:
        {
            Result.Components = 3;
            Result.ComponentByteSize = sizeof(f32);
        } break;

        case PixelFormat_BGR8:
        {
            Result.Components = 3;
            Result.ComponentByteSize = sizeof(u8);
        } break;
    }

    return(Result);
}

struct render_target
{
    u32 Width;
    u32 Height;

    pixel_format Format;
    pixel_format_info FormatInfo;

    byte_buffer Buffer;
};

function render_target
CreateRenderTarget(u32 Width, u32 Height, pixel_format Format, memory_arena *Arena = &DefaultArena)
{
    render_target Result = {};
    Result.Width = Width;
    Result.Height = Height;
    Result.Format = Format;
    Result.FormatInfo = GetPixelFormatInfo(Format);

    u32 PixelByteSize = Result.FormatInfo.Components * Result.FormatInfo.ComponentByteSize;
    u32 TotalAllocationSize = PixelByteSize * Result.Width * Result.Height;
    Result.Buffer = PushByteBuffer(TotalAllocationSize);
    return(Result);
}

#pragma pack(push, 1)
struct bitmap_header 
{
    u16 Type;
    u32 FileSize;
    u16 Reserved1;
    u16 Reserved2;
    u32 OffsetBytes;
    
    // BITMAP INFO HEADER
    u32 StructureSize;
    s32 Width;
    s32 Height;
    u16 Planes;
    u16 BitsPerPixel;
    u32 Compression;
    u32 SizeImage;
    s32 HorizontalPixelsPerMeter;
    s32 VerticalPixelsPerMeter;
    u32 ColorIndicesUsed;
    u32 ColorIndicesImportant;
};
#pragma pack(pop)

struct bitmap_image
{
    bitmap_header Header;
    
    u32 Width;
    u32 Height;
    byte_buffer Data;
};

struct viewport
{
    f32 Width;
    f32 Height;
    f32 HalfWidth;
    f32 HalfHeight;

    vec4 ViewportCenter;
};

struct camera
{
    f32 NearPlane;
    f32 FarPlane;
    f32 AspectRatio;

    f32 Gamma;
    
    vec4 CameraX;
    vec4 CameraY;
    vec4 CameraZ;

    vec4 Position;
    vec4 Target;

    viewport Viewport;
};

struct job_data
{
    f32 ViewportX;
    f32 ViewportY;
    u32 PixelIndex;
};

enum entity_type
{
    EntityType_Unknown,
    
    EntityType_Sphere,
    EntityType_Plane,
};

struct hit_info
{
    vec4 HitPosition;
    vec4 HitSurfaceNormal;
    u32 MaterialIndex;
    b32 MadeContact;
    
    u32 EntityIndex;
    entity_type EntityType;
};

struct raytracer_pixel_info
{
    f32 Width;
    f32 Height;
    f32 HalfWidth;
    f32 HalfHeight;
};

struct raytracer_statistics
{
    f32 AverageTimePerPixel_MS;
    f32 AverageTimePerInitialRay_MS;
    f32 AverageTimePerRayBounce_MS;

    f32 TotalRenderTime_S;
    u32 TotalPassedIntersectionTests;
    u32 TotalValidIntersectionTests;
    u32 TotalIntersectionTests;
    u32 TotalPixelsTraced;
    u32 AverageNumberOfBouncesPerPixel;
    u32 CurrentRaytraceProgress;
};

struct raytracer
{
    u32 RaysPerPixel;
    u32 BouncesPerRay;
    u32 TotalFrames;
    u32 CurrentFrame;
    u32 TotalPixelsToTrace;

    u32 RenderWidth;
    u32 RenderHeight;

    render_target RenderOutputTarget;
    render_target BloomOutputTarget;

    camera Camera;
    job_queue ProcessingQueue;
    world_storage WorldStorage;
    ticket_mutex GlobalMutex;

    raytracer_pixel_info PixelInfo;
    raytracer_statistics Statistics;
};

enum gaussianblur_type
{
    GaussianBlur_Horizontal,
    GaussianBlur_Vertical
};

struct image_pixel_data
{
    u16 B, G, R, A;
};