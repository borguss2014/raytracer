#pragma once

#include <stdint.h>
#include <cstddef>
#include <limits.h>
#include <intrin.h>
#include <float.h>

typedef uint8_t  b8;
typedef uint16_t b16;
typedef uint32_t b32;
typedef uint64_t b64;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t   s8;
typedef uint16_t s16;
typedef int32_t  s32;
typedef int64_t  s64;

typedef float  real32;
typedef double real64;

typedef real32 f32;
typedef real32 r32;

typedef real64 f64;
typedef real64 r64;

typedef size_t memory_size;
typedef memory_size mmsz;
typedef uintptr_t ump;
typedef intptr_t smp;

#define U32Max UINT_MAX
#define F32Max FLT_MAX
#define F32Min -FLT_MAX

#define INFINITE_TIMEOUT U32Max

#define PI 3.14159265358979323846
#define TWO_PI 6.28318530718
#define HALF_PI 1.57079632679

#define PI_OVER_HALF_CIRCLE_DEGREES 0.01745329251994329576923690768489f
#define HALF_CIRCLE_DEGREES_OVER_PI 57.295779513082320876798154814105f

#define Kilobytes(Value) (Value * 1024LL)
#define Megabytes(Value) (Kilobytes(Value) * 1024LL)
#define Gigabytes(Value) (Megabytes(Value) * 1024LL)
#define Terabytes(Value) (Gigabytes(Value) * 1024LL)

#define function static
#define local_persist static
#define global_variable static

#define enum32(type) u32

struct byte_buffer
{
    u8 *Data;
    mmsz Capacity;

    u8 * operator[](u32 Index)
    {
        Assert(Index < Capacity);

        u8 *Result = &Data[Index];
        return(Result);
    }
};