#if defined(Optimization_Debug)
    #ifndef AssertMsg
        #define AssertMsg(Cond, Msg, ...) do { \
                if (!(Cond)) { \
                    DebugTrap(); \
                } \
            } while (0)
    #endif

    #ifndef Assert
        #define Assert(Cond) AssertMsg(Cond, 0)
    #endif

    #if defined(_MSC_VER)
        #if _MSC_VER < 1300
            #define DebugTrap() __asm int 3 /* Trap to debugger! */
        #else
            #define DebugTrap() __debugbreak()
        #endif
    #else
        #define DebugTrap() __builtin_trap()
    #endif

#else
    #define DebugTrap()
    #define Assert(Cond)
    #define AssertMsg(Cond, Msg, ...)
#endif

#include "application_types.h"

#define ArrayCapacity(Array) (sizeof(Array) / sizeof((Array)[0]))
#define OffsetOf(StructType, MemberType) (mmsz)(&(((StructType *)0)->MemberType))

// Helper function to count set bits in the processor mask.
// https://learn.microsoft.com/en-us/windows/win32/api/sysinfoapi/nf-sysinfoapi-getlogicalprocessorinformation
function u32 
CountSetBits(ump BitMask)
{
    u32 BitSetCount = 0;
    u32 LSHIFT = sizeof(ump) * 8 - 1;
    ump BitTest = (ump)1 << LSHIFT;
    for (u32 Index = 0; Index <= LSHIFT; ++Index)
    {
        BitSetCount += ((BitMask & BitTest) ? 1 : 0);
        BitTest /= 2;
    }

    return BitSetCount;
}

enum timestamps_comparison
{
    Timestamps_Equal = 0,

    Timestamps_FirstIsEarlier,
    Timestamps_FirstIsLater
};

struct platform_memory_block
{
	u64 Flags;
    ump Base;
    mmsz Size;
    mmsz Used;
    platform_memory_block *PrevBlock;
};

enum thread_creation_state
{
    ThreadCreationState_RunImmediately = 0,

    ThreadCreationState_CreateSuspended,
    ThreadCreationState_StackSizeReservation
};

struct thread_creation_args
{
    void *StartAddress;
    void *ThreadParams;
    mmsz StackSize;
    thread_creation_state CreationState;
};

struct thread_info
{
    void *Handle;
    u32 ThreadID;
};

struct payload
{
    void *Data;

    u32 Offset;
    u32 BatchCount;
};

#define MAIN_THREAD 0

#define JOB_QUEUE_ENTRY_CALLBACK(name) void name(payload Payload, u32 ParentThreadIndex, u32 ProcessingThreadIndex)
typedef JOB_QUEUE_ENTRY_CALLBACK(job_queue_callback);

struct job_queue_entry
{
    payload Payload;
    u32 CallerThreadIndex;
    job_queue_callback *Callback;
};

struct job_queue
{
    void *SemaphoreHandle;

    u32 ThreadCount;
    
    u32 volatile AvailableEntriesCount;
    
    // TODO(Cristian): Implement a circular FIFO queue
    u32 volatile NextJobEntryToRead;
    u32 volatile NextJobEntryToWrite;

    job_queue_entry *JobEntries;
    u32 JobEntriesCapacity;
};

struct thread_params
{
    job_queue *Queue;
    u32 LogicalThreadIndex;
};

struct timer
{
    b32 IsRunning;
    f64 Timestamp;
};

struct memory_arena;

// ============PLATFORM CALLBACKS============
#define PLATFORM_COPY_FILE(name) b32 name(const char *Src, const char *Dst, b32 FailIfExists)
typedef PLATFORM_COPY_FILE(platform_copy_file);

#define PLATFORM_CHECK_IF_FILE_EXISTS(name) b32 name(const char *Path)
typedef PLATFORM_CHECK_IF_FILE_EXISTS(platform_check_if_file_exists);

#define PLATFORM_GET_FILE_LAST_WRITE_TIME(name) u64 name(const char *Path)
typedef PLATFORM_GET_FILE_LAST_WRITE_TIME(platform_get_file_last_write_time);

#define PLATFORM_ALLOCATE_MEMORY(name) platform_memory_block *name(mmsz Size, u32 AllocationFlags)
typedef PLATFORM_ALLOCATE_MEMORY(platform_allocate_memory);

#define PLATFORM_DEALLOCATE_MEMORY(name) void name(platform_memory_block *PlatformBlock)
typedef PLATFORM_DEALLOCATE_MEMORY(platform_deallocate_memory);

#define PLATFORM_ZERO_MEMORY_BLOCK(name) void * name(void *Block, memory_size Size)
typedef PLATFORM_ZERO_MEMORY_BLOCK(platform_zero_memory_block);

#define PLATFORM_COPY_MEMORY(name) void name(void *Dest, mmsz DestSize, void *Src, mmsz SrcSize)
typedef PLATFORM_COPY_MEMORY(platform_copy_memory);

#define PLATFORM_ATOMIC_EXCHANGE_U32(name) u32 name(u32 volatile *Value, u32 New)
typedef PLATFORM_ATOMIC_EXCHANGE_U32(platform_atomic_exchange_u32);

#define PLATFORM_ATOMIC_ADD_U32(name) u32 name(u32 volatile *Value, u32 Addend)
typedef PLATFORM_ATOMIC_ADD_U32(platform_atomic_add_u32);

#define PLATFORM_ATOMIC_COMPARE_EXCHANGE_U32(name) u32 name(u32 volatile *Destination, u32 Exchange, u32 Comparand)
typedef PLATFORM_ATOMIC_COMPARE_EXCHANGE_U32(platform_atomic_compare_exchange_u32);

#define PLATFORM_ATOMIC_INCREMENT_U32(name) u32 name(u32 volatile *Addend)
typedef PLATFORM_ATOMIC_INCREMENT_U32(platform_atomic_increment_u32);

#define PLATFORM_ATOMIC_DECREMENT_U32(name) u32 name(u32 volatile *Addend)
typedef PLATFORM_ATOMIC_DECREMENT_U32(platform_atomic_decrement_u32);


#define PLATFORM_ATOMIC_EXCHANGE_U64(name) u64 name(u64 volatile *Value, u64 New)
typedef PLATFORM_ATOMIC_EXCHANGE_U64(platform_atomic_exchange_u64);

#define PLATFORM_ATOMIC_ADD_U64(name) u64 name(u64 volatile *Value, u64 Addend)
typedef PLATFORM_ATOMIC_ADD_U64(platform_atomic_add_u64);

#define PLATFORM_ATOMIC_COMPARE_EXCHANGE_U64(name) u64 name(u64 volatile *Destination, u64 Exchange, u64 Comparand)
typedef PLATFORM_ATOMIC_COMPARE_EXCHANGE_U64(platform_atomic_compare_exchange_u64);

#define PLATFORM_ATOMIC_INCREMENT_U64(name) u64 name(u64 volatile *Addend)
typedef PLATFORM_ATOMIC_INCREMENT_U64(platform_atomic_increment_u64);

#define PLATFORM_ATOMIC_DECREMENT_U64(name) u64 name(u64 volatile *Addend)
typedef PLATFORM_ATOMIC_DECREMENT_U64(platform_atomic_decrement_u64);

#define PLATFORM_CREATE_SEMAPHORE(name) void * name(s32 InitialCount, \
                                                    s32 MaximumCount, \
                                                    const char *Name)
typedef PLATFORM_CREATE_SEMAPHORE(platform_create_semaphore);

#define PLATFORM_SEMAPHORE_WAIT(name) s32 name(void *Handle, s32 dwMilliseconds, b32 bAlertable)
typedef PLATFORM_SEMAPHORE_WAIT(platform_semaphore_wait);

#define PLATFORM_RELEASE_SEMAPHORE(name) b32 name(void *Handle, s32 ReleaseCount, s32 *PreviousCount)
typedef PLATFORM_RELEASE_SEMAPHORE(platform_release_semaphore);

#define PLATFORM_GET_LOGICAL_PROCESSOR_COUNT(name) s32 name(void)
typedef PLATFORM_GET_LOGICAL_PROCESSOR_COUNT(platform_get_logical_processor_count);

// NOTE(Cristian): StackSize not used unless ThreadCreationState_StackSizeReservation is specified as thread creation state
#define PLATFORM_CREATE_THREAD(name) thread_info name(thread_creation_args CreationArgs)
typedef PLATFORM_CREATE_THREAD(platform_create_thread);

#define PLATFORM_CLOSE_HANDLE(name) b32 name(void *Handle)
typedef PLATFORM_CLOSE_HANDLE(platform_close_handle);

#define PLATFORM_CREATE_JOB_QUEUE(name) void name(job_queue *Queue)
typedef PLATFORM_CREATE_JOB_QUEUE(platform_create_job_queue);

#define PLATFORM_DO_NEXT_AVAILABLE_JOB_ENTRY(name) b32 name(job_queue *Queue, u32 CallerThreadIndex)
typedef PLATFORM_DO_NEXT_AVAILABLE_JOB_ENTRY(platform_do_next_available_job_entry);

#define PLATFORM_ADD_JOB_ENTRY(name) void name(job_queue *Queue, job_queue_callback *Callback, payload Payload, u32 CallerThreadIndex)
typedef PLATFORM_ADD_JOB_ENTRY(platform_add_job_entry);

#define PLATFORM_COMPLETE_ALL_JOB_QUEUE_WORK(name) void name(job_queue *Queue, u32 CallerThreadIndex)
typedef PLATFORM_COMPLETE_ALL_JOB_QUEUE_WORK(platform_complete_all_job_queue_work);

#define PLATFORM_GET_TIMESTAMP(name) f64 name(void)
typedef PLATFORM_GET_TIMESTAMP(platform_get_timestamp);

#define PLATFORM_GET_TIME_IN_SECONDS(name) f64 name(f64 Timestamp)
typedef PLATFORM_GET_TIME_IN_SECONDS(platform_get_time_in_seconds);
// ==========================================

typedef u32 platform_thread_proc(void *lpParameter);

struct platform_api
{
    platform_copy_file                      *CopyFile;
    platform_check_if_file_exists           *CheckIfFileExists;
    platform_get_file_last_write_time       *GetFileLastWriteTime;
    platform_allocate_memory                *AllocateMemory;
    platform_deallocate_memory              *DeallocateMemory;
    platform_zero_memory_block              *ZeroMemoryBlock;
    platform_copy_memory                    *CopyMemory;

    platform_atomic_exchange_u32            *AtomicExchangeU32;
    platform_atomic_add_u32                 *AtomicAddU32;
    platform_atomic_compare_exchange_u32    *AtomicCompareExchangeU32;
    platform_atomic_increment_u32           *AtomicIncrementU32;
    platform_atomic_decrement_u32           *AtomicDecrementU32;

    platform_atomic_exchange_u64            *AtomicExchangeU64;
    platform_atomic_add_u64                 *AtomicAddU64;
    platform_atomic_compare_exchange_u64    *AtomicCompareExchangeU64;
    platform_atomic_increment_u64           *AtomicIncrementU64;
    platform_atomic_decrement_u64           *AtomicDecrementU64;

    platform_create_semaphore               *CreateSemaphore;
    platform_semaphore_wait                 *SemaphoreWait;
    platform_release_semaphore              *ReleaseSemaphore;

    platform_get_logical_processor_count    *GetLogicalProcessorCount;
    platform_create_thread                  *CreateThread;
    platform_close_handle                   *CloseHandle;

    platform_create_job_queue               *CreateJobQueue;
    platform_do_next_available_job_entry    *DoNextAvailableJobEntry;
    platform_add_job_entry                  *AddJobEntry;
    platform_complete_all_job_queue_work    *CompleteAllJobQueueWork;

    platform_get_timestamp                  *GetTimestamp;
    platform_get_time_in_seconds            *GetTimeInSeconds;
};

struct platform_memory
{
    b32 ApplicationRunning;
    platform_api PlatformAPI;
    memory_arena *PlatformArena;
};

enum platform_memory_block_flags
{
    PlatformFlags_UnderflowCheck = 0x01,
    PlatformFlags_OverflowCheck  = 0x02
};

struct ticket_mutex
{
    u64 volatile CurrentTicket;
    u64 volatile ServedTicket;
};

#define APP_INIT(name) void name(platform_memory *PlatformMemory)
typedef APP_INIT(app_init);

#define APP_UPDATE_AND_RENDER(name) void name(platform_memory *PlatformMemory, f64 DeltaTime)
typedef APP_UPDATE_AND_RENDER(app_update_and_render);

#define APP_SHUTDOWN(name) void name()
typedef APP_SHUTDOWN(app_shutdown);

extern platform_api Platform;

function void
TicketMutexLock(ticket_mutex *Mutex)
{
    u64 TicketNumber = Platform.AtomicAddU64((u64 volatile *)&Mutex->CurrentTicket, 1);
    while (TicketNumber != Mutex->ServedTicket); // TODO(Cristian): _mm_pause here?
}

function void
TicketMutexUnlock(ticket_mutex *Mutex)
{
    Platform.AtomicAddU64((u64 volatile *)&Mutex->ServedTicket, 1);
}

function void
StartTimer(timer *Timer)
{
    Timer->IsRunning = true;
    Timer->Timestamp = Platform.GetTimestamp();
}

function void
StopTimer(timer *Timer)
{
    Timer->IsRunning = false;
    Timer->Timestamp = Platform.GetTimestamp() - Timer->Timestamp;
}

function void
ResetTimer(timer *Timer)
{
    Timer->Timestamp = Platform.GetTimestamp();
}

function f32
GetElapsedTimeInSeconds(timer *Timer)
{
    f64 TimeDifference = Platform.GetTimestamp() - Timer->Timestamp;
    f32 Result = (f32)Platform.GetTimeInSeconds(TimeDifference);
    return(Result);
}

function f32
GetElapsedTimeInMilliseconds(timer *Timer)
{
    f32 Result = GetElapsedTimeInSeconds(Timer) * 1000;
    return(Result);
}

function f32
GetTimeInMilliseconds(timer *Timer)
{
    f32 Result = (f32)Platform.GetTimeInSeconds(Timer->Timestamp) * 1000;
    return(Result);
}