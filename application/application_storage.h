extern memory_arena DefaultArena;

struct sphere
{
    vec4 Center;
    f32 Radius;
    u32 MaterialIndex;
};

struct material
{
    vec4 Albedo;
    vec4 Emissive;
    f32 EmissiveStrength;
    f32 Metalness;
    f32 Roughness;
};

struct plane
{
    vec4 Origin;
    vec4 Normal;
    u32 MaterialIndex;
};

struct point_light
{
    vec4 Position;
    vec4 Color;
    f32 Intensity;
    f32 FallOff;
    f32 Radius;
};

struct directional_light
{
    vec4 Direction;
    vec4 Color;
};

enum storage_type
{
    StorageType_None = 0, // Allows for the existence of a stub storage
    
    StorageType_Material,
    StorageType_Sphere,
    StorageType_Plane,
    StorageType_PointLight,
    StorageType_DirectionalLight,
    StorageType_COUNT
};

struct storage
{
    storage_type Type;
    mmsz TypeSize;
    
    byte_buffer Buffer;
    u32 Used;
};

struct world_storage
{
    storage StorageUnits[StorageType_COUNT];
};

struct storage_iterator
{
    u8 *Position;
    mmsz StepSize;
};

function storage_iterator
CreateStorageIterator(storage *Storage)
{
    Assert(Storage);
    
    storage_iterator Result = {};
    Result.Position = Storage->Buffer.Data;
    Result.StepSize = Storage->TypeSize;
    return(Result);
}

function void
AdvanceStorageIterator(storage_iterator *Iterator)
{
    Assert(Iterator->Position);
    
    Iterator->Position += Iterator->StepSize;
}

#define GetNextStorageItem(Iterator, Type) (Type *)GetNextStorageItem_(Iterator)

function void *
GetNextStorageItem_(storage_iterator *Iterator)
{
    Assert(Iterator);
    
    void *Result = Iterator->Position;
    AdvanceStorageIterator(Iterator);
    return(Result);
}

function storage *
GetStorageType(world_storage *Storage, storage_type Type)
{
    storage *Result = &Storage->StorageUnits[Type];
    return(Result);
}

#define AddStorageType(Storage, StorageType, Type, Capacity) AddStorageType_(Storage, StorageType, sizeof(Type), Capacity)
#define AddStorageTypeWithArena(Storage, StorageType, Type, Capacity, Arena) AddStorageType_(Storage, StorageType, sizeof(Type), Capacity, Arena)

function void
AddStorageType_(world_storage *WorldStorage, storage_type StorageType, mmsz TypeSize, u32 Capacity, memory_arena *Arena = &DefaultArena)
{
    mmsz AllocationSize = TypeSize * Capacity;
    
    storage *Storage = &WorldStorage->StorageUnits[StorageType];
    Storage->Buffer.Data = (u8 *)PushSlice(Arena, AllocationSize);
    Storage->Buffer.Capacity = AllocationSize;
    Storage->Type = StorageType;
    Storage->TypeSize = TypeSize;
}

function world_storage
CreateWorldStorage(memory_arena *Arena = &DefaultArena)
{
    world_storage Result = {};
    AddStorageType(&Result, StorageType_Material, material, 32);
    AddStorageType(&Result, StorageType_Sphere, sphere, 10);
    AddStorageType(&Result, StorageType_Plane, plane, 10);
    AddStorageType(&Result, StorageType_PointLight, point_light, 10);
    AddStorageType(&Result, StorageType_DirectionalLight, directional_light, 2);
    return(Result);
}

#define PushToStorage(Storage, Type) (Type *)PushToStorage_(Storage, sizeof(Type))

function void *
PushToStorage_(storage *Storage, mmsz TypeSize)
{
    Assert(Storage->Used * TypeSize < Storage->Buffer.Capacity);
    
    void *Result = Storage->Buffer.Data + (Storage->Used * TypeSize);
    return(Result);
}

#define GetFromStorageAtIndex(Storage, Index, Type) (Type *)GetFromStorageAtIndex_(Storage, Index, sizeof(Type))

function void *
GetFromStorageAtIndex_(storage *Storage, u32 Index, u32 TypeSize)
{
    Assert(Storage->Buffer.Data);
    Assert((Index * TypeSize) < Storage->Buffer.Capacity);
    
    void *Result = Storage->Buffer.Data + (Index * TypeSize);
    return(Result);
}