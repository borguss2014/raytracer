struct memory_arena
{
    platform_memory_block *CurrentBlock;
    u32 AllocationFlags;
    mmsz MinimumBlockSize;

    platform_memory_block *TemporaryBlock;
    mmsz TemporaryUsage;
};

struct arena_push_params
{
    u32 Flags;
    u32 Alignment;
    mmsz MinimumBlockSize;
};

function arena_push_params
DefaultPushParams(void)
{
    arena_push_params Params = {};
    Params.Alignment = 4;
    Params.MinimumBlockSize = Megabytes(1);
    
    return(Params);
}

function b8
IsPow2(u32 Value)
{
    b8 Result = false;
    
    u32 Mask = Value - 1;
    if (!(Value & Mask))
    {
        Result = true;
    }
    
    return(Result);
}

function memory_size
GetAlignmentOffset(memory_arena *Arena, memory_size Alignment)
{
    memory_size ResultPointer = (memory_size)Arena->CurrentBlock->Base + Arena->CurrentBlock->Used;
    memory_size AlignmentMask = Alignment - 1;
    memory_size BitOpResult = ResultPointer & AlignmentMask;
    
    memory_size AlignmentOffset = 0;
    if (BitOpResult)
    {
        AlignmentOffset = Alignment - BitOpResult;
    }
    
    return(AlignmentOffset);
}

function memory_size
AlignToPow2(memory_size Size, memory_size Pow2Alignment)
{
    memory_size AlignmentOffset = 0;
    
    memory_size AlignmentMask = Pow2Alignment - 1;
    memory_size BitOpResult = Size & AlignmentMask;
    if (BitOpResult)
    {
        AlignmentOffset = Pow2Alignment - BitOpResult;
    }
    
    memory_size AlignedSize = Size + AlignmentOffset;
    return(AlignedSize);
}

function memory_size
GetEffectiveSize(memory_arena *Arena, memory_size SizeInit, memory_size Alignment)
{
    memory_size Size = SizeInit;
    
    memory_size AlignmentOffset = GetAlignmentOffset(Arena, Alignment);
    Size += AlignmentOffset;
    
    return(Size);
}

#define PushStruct(Arena, Type) (Type *) PushSize_(Arena, sizeof(Type))
#define PushArray(Arena, Count, Type) (Type *) PushSize_(Arena, sizeof(Type) * Count)
#define PushSlice(Arena, Size) PushSize_(Arena, Size)

function void
InitializeArena(memory_arena *Arena, arena_push_params Params = DefaultPushParams())
{
    if(!Arena->CurrentBlock)
    {
        Assert(Params.Alignment <= 128);
        Assert(IsPow2(Params.Alignment));
        
        Arena->MinimumBlockSize = Params.MinimumBlockSize;
        
        platform_memory_block *Block = Platform.AllocateMemory(Arena->MinimumBlockSize, Arena->AllocationFlags);
        Block->PrevBlock = Arena->CurrentBlock;
        Arena->CurrentBlock = Block;
    }
}

function void *
PushSize_(memory_arena *Arena, memory_size SizeInit, arena_push_params Params = DefaultPushParams())
{
    void *Result = 0;
    
    Assert(Params.Alignment <= 128);
    Assert(IsPow2(Params.Alignment));
    
    memory_size Size = 0;
    if(Arena->CurrentBlock)
    {
        Size = GetEffectiveSize(Arena, SizeInit, Params.Alignment);
    }
    
    if(!Arena->CurrentBlock || 
       ((Arena->CurrentBlock->Used + Size) > Arena->CurrentBlock->Size))
    {
        Size = SizeInit;
        
        if(Arena->AllocationFlags & (PlatformFlags_UnderflowCheck | PlatformFlags_OverflowCheck))
        {
            Arena->MinimumBlockSize = 0;
            Size = AlignToPow2(Size, Params.Alignment);
        }
        else
        {
            Arena->MinimumBlockSize = Params.MinimumBlockSize;
        }
        
        memory_size AllocationSize = (Size > Arena->MinimumBlockSize) ? SizeInit : Arena->MinimumBlockSize;
        
        platform_memory_block *Block = Platform.AllocateMemory(AllocationSize, Arena->AllocationFlags);
        Block->PrevBlock = Arena->CurrentBlock;
        Arena->CurrentBlock = Block;
    }
    
    memory_size AlignmentOffset = GetAlignmentOffset(Arena, Params.Alignment);
    memory_size OffsetInBlock = Arena->CurrentBlock->Used + AlignmentOffset;
    
    Result = (void *)((ump)Arena->CurrentBlock->Base + OffsetInBlock);
    Arena->CurrentBlock->Used += Size;
    
    Assert(Size >= SizeInit);
    Assert(Arena->CurrentBlock->Used <= Arena->CurrentBlock->Size);
    
    return(Result);
}

function void
BeginTemporaryMemory(memory_arena *Arena, b32 InitArena = false)
{
    // TODO(Cristian): Should maybe always initialize arenas...?
    if(InitArena && !Arena->CurrentBlock)
    {
        InitializeArena(Arena);
    }

    Arena->TemporaryBlock = Arena->CurrentBlock;
    Arena->TemporaryUsage = Arena->CurrentBlock->Used;
}

function void
FreeLastMemoryBlock(memory_arena *Arena)
{
    platform_memory_block *FreeBlock = Arena->CurrentBlock;
    Arena->CurrentBlock = FreeBlock->PrevBlock;
    Platform.DeallocateMemory(FreeBlock);
}

function void
EndTemporaryMemory(memory_arena *Arena)
{    
    while(Arena->CurrentBlock != Arena->TemporaryBlock)
    {
        FreeLastMemoryBlock(Arena);
    }
    
    if(Arena->CurrentBlock)
    {
        Assert(Arena->CurrentBlock->Used >= Arena->TemporaryUsage);
        Arena->CurrentBlock->Used = Arena->TemporaryUsage;

        // NOTE(Cristian): We assume memory is always zero-initialized when received from the OS.
        // Temporary memory functionality breaks this assumption by writing to the memory block.
        // Hence, we need to also clear any temporary writes at the end of temporary memory operations.
        void *BlockToClearPtr = (void *)((ump)Arena->CurrentBlock->Base + Arena->CurrentBlock->Used);
        memory_size ClearToZeroSize = Arena->CurrentBlock->Size - Arena->CurrentBlock->Used;
        Platform.ZeroMemoryBlock(BlockToClearPtr, ClearToZeroSize);
    }
}

function void
CommitTemporaryMemory(memory_arena *Arena)
{
    Arena->TemporaryBlock = 0;
    Arena->TemporaryUsage = 0;
}

function void
ClearArena(memory_arena *Arena)
{
    platform_memory_block *PreviousBlock = Arena->CurrentBlock->PrevBlock;
    while(PreviousBlock)
    {
        platform_memory_block *FreeBlock = Arena->CurrentBlock;
        Arena->CurrentBlock = PreviousBlock;
        Platform.DeallocateMemory(FreeBlock);
        
        PreviousBlock = Arena->CurrentBlock->PrevBlock;
    }
    
    Assert(Arena->CurrentBlock);
    
    Arena->CurrentBlock->Used = 0;
}