#include "math.h"
#include <stdlib.h>

#define DegreesToRadians(degrees) (degrees * PI_OVER_HALF_CIRCLE_DEGREES)
#define RadiansToDegrees(radians) (radians * HALF_CIRCLE_DEGREES_OVER_PI)

#define Cube(x) ((x)*(x)*(x))
#define Square(x) ((x)*(x))

union vec3_u8
{
    struct { u8 x, y, z; };
    struct { u8 r, g, b; };
    struct { u8 s, t, p; };
    u8 Data[3];
};

union vec3
{
    struct { f32 x, y, z; };
    struct { f32 r, g, b; };
    struct { f32 s, t, p; };
    f32 Data[3];
};

union vec4_u8
{
    struct { u8 x, y, z, w; };
    struct { u8 r, g, b, a; };
    struct { u8 s, t, p, q; };
    u8 Data[4];
};

union vec4
{
    struct { f32 x, y, z, w; };
    struct { f32 r, g, b, a; };
    struct { f32 s, t, p, q; };
    f32 Data[4];
};

// VEC3_U8 INIT
inline vec3_u8
V3(u8 X)
{
    vec3_u8 Result = {};
    Result.x = X;
    Result.y = X;
    Result.z = X;
    
    return(Result);
}

inline vec3_u8
V3(u8 X, u8 Y, u8 Z)
{
    vec3_u8 Result = {};
    Result.x = X;
    Result.y = Y;
    Result.z = Z;
    
    return(Result);
}

inline vec3_u8
V3(vec4_u8 V)
{
    vec3_u8 Result = {};
    Result.x = V.x;
    Result.y = V.y;
    Result.z = V.z;

    return(Result);
}

// VEC3 INIT
inline vec3
V3(f32 X)
{
    vec3 Result = {};
    Result.x = X;
    Result.y = X;
    Result.z = X;
    
    return(Result);
}

inline vec3
V3(f32 X, f32 Y, f32 Z)
{
    vec3 Result = {};
    Result.x = X;
    Result.y = Y;
    Result.z = Z;
    
    return(Result);
}

inline vec3
V3(vec4 V)
{
    vec3 Result = {};
    Result.x = V.x;
    Result.y = V.y;
    Result.z = V.z;

    return(Result);
}

// VEC4_U8 INIT
inline vec4_u8
V4(u8 X)
{
    vec4_u8 Result = {};
    Result.x = X;
    Result.y = X;
    Result.z = X;
    Result.w = X;
    
    return(Result);
}

inline vec4_u8
V4(u8 X, u8 Y, u8 Z, u8 W)
{
    vec4_u8 Result = {};
    Result.x = X;
    Result.y = Y;
    Result.z = Z;
    Result.w = W;
    
    return(Result);
}

inline vec4_u8
V4(vec3_u8 V)
{
    vec4_u8 Result = {};
    Result.x = V.x;
    Result.y = V.y;
    Result.z = V.z;

    return(Result);
}

inline vec4_u8
V4(vec3_u8 V, u8 W)
{
    vec4_u8 Result = {};
    Result.x = V.x;
    Result.y = V.y;
    Result.z = V.z;
    Result.w = W;

    return(Result);
}

// VEC4 INIT
inline vec4
V4(f32 X)
{
    vec4 Result = {};
    Result.x = X;
    Result.y = X;
    Result.z = X;
    Result.w = X;
    
    return(Result);
}

inline vec4
V4(f32 X, f32 Y, f32 Z, f32 W)
{
    vec4 Result = {};
    Result.x = X;
    Result.y = Y;
    Result.z = Z;
    Result.w = W;
    
    return(Result);
}

inline vec4
V4(vec3 V)
{
    vec4 Result = {};
    Result.x = V.x;
    Result.y = V.y;
    Result.z = V.z;

    return(Result);
}

inline vec4
V4(vec3 V, f32 W)
{
    vec4 Result = {};
    Result.x = V.x;
    Result.y = V.y;
    Result.z = V.z;
    Result.w = W;

    return(Result);
}

// VEC3 OPERATORS
inline vec3
operator+(vec3 A, f32 B)
{
    vec3 Result = A;
    Result.x += B;
    Result.y += B;
    Result.z += B;
    return(Result);
}

inline vec3
operator+(f32 A, vec3 B)
{
    vec3 Result = B;
    Result.x += A;
    Result.y += A;
    Result.z += A;
    return(Result);
}

inline vec3
operator-(vec3 A, f32 B)
{
    vec3 Result = A;
    Result.x -= B;
    Result.y -= B;
    Result.z -= B;
    return(Result);
}

inline vec3
operator-(f32 A, vec3 B)
{
    vec3 Result = B;
    Result.x = A - Result.x;
    Result.y = A - Result.y;
    Result.z = A - Result.z;
    return(Result);
}

inline vec3
operator*(vec3 A, f32 B)
{
    vec3 Result = A;
    Result.x *= B;
    Result.y *= B;
    Result.z *= B;
    return(Result);
}

inline vec3
operator*(f32 A, vec3 B)
{
    vec3 Result = B;
    Result.x *= A;
    Result.y *= A;
    Result.z *= A;
    return(Result);
}

inline vec3
operator/(vec3 A, f32 B)
{
    vec3 Result = A;
    Result.x /= B;
    Result.y /= B;
    Result.z /= B;
    return(Result);
}

inline vec3
operator/(f32 A, vec3 B)
{
    vec3 Result = B;
    Result.x = A / Result.x;
    Result.y = A / Result.y;
    Result.z = A / Result.z;
    return(Result);
}

inline void
operator+=(vec3 &A, f32 B)
{
    A = A + B;
}

inline void
operator-=(vec3 &A, f32 B)
{
    A = A - B;
}

inline void
operator*=(vec3 &A, f32 B)
{
    A = A * B;
}

inline void
operator/=(vec3 &A, f32 B)
{
    A = A / B;
}

inline vec3
operator+(vec3 A, vec3 B)
{
    vec3 Result;
    Result.x = A.x + B.x;
    Result.y = A.y + B.y;
    Result.z = A.z + B.z;
    return(Result);
}

inline vec3
operator-(vec3 A, vec3 B)
{
    vec3 Result;
    Result.x = A.x - B.x;
    Result.y = A.y - B.y;
    Result.z = A.z - B.z;
    return(Result);
}

inline vec3
operator*(vec3 A, vec3 B)
{
    vec3 Result;
    Result.x = A.x * B.x;
    Result.y = A.y * B.y;
    Result.z = A.z * B.z;
    return(Result);
}

inline vec3
operator/(vec3 A, vec3 B)
{
    vec3 Result;
    Result.x = A.x / B.x;
    Result.y = A.y / B.y;
    Result.z = A.z / B.z;
    return(Result);
}

inline void
operator+=(vec3 &A, vec3 B)
{
    A = A + B;
}

inline void
operator-=(vec3 &A, vec3 B)
{
    A = A - B;
}

inline void
operator*=(vec3 &A, vec3 B)
{
    A = A * B;
}

inline void
operator/=(vec3 &A, vec3 B)
{
    A = A / B;
}

inline b32
operator==(vec3 A, vec3 B)
{
    b32 Result = false;
    if((A.x == B.x) &&
       (A.y == B.y) &&
       (A.z == B.z))
    {
        Result = true;
    }
    return(Result);
}

inline b32
operator!=(vec3 A, vec3 B)
{
    b32 Result = false;
    if((A.x != B.x) ||
       (A.y != B.y) ||
       (A.z != B.z))
    {
        Result = true;
    }
    return(Result);
}

// VEC4 OPERATORS
inline vec4
operator+(vec4 A, f32 B)
{
    vec4 Result = A;
    Result.x += B;
    Result.y += B;
    Result.z += B;
    Result.w += B;
    return(Result);
}

inline vec4
operator+(f32 A, vec4 B)
{
    vec4 Result = B;
    Result.x += A;
    Result.y += A;
    Result.z += A;
    Result.w += A;
    return(Result);
}

inline vec4
operator-(vec4 A, f32 B)
{
    vec4 Result = A;
    Result.x -= B;
    Result.y -= B;
    Result.z -= B;
    Result.w -= B;
    return(Result);
}

inline vec4
operator-(f32 A, vec4 B)
{
    vec4 Result = B;
    Result.x = A - Result.x;
    Result.y = A - Result.y;
    Result.z = A - Result.z;
    Result.w = A - Result.w;
    return(Result);
}

inline vec4
operator*(vec4 A, f32 B)
{
    vec4 Result = A;
    Result.x *= B;
    Result.y *= B;
    Result.z *= B;
    Result.w *= B;
    return(Result);
}

inline vec4
operator*(f32 A, vec4 B)
{
    vec4 Result = B;
    Result.x *= A;
    Result.y *= A;
    Result.z *= A;
    Result.w *= A;
    return(Result);
}

inline vec4
operator/(vec4 A, f32 B)
{
    vec4 Result = A;
    Result.x /= B;
    Result.y /= B;
    Result.z /= B;
    Result.w /= B;
    return(Result);
}

inline vec4
operator/(f32 A, vec4 B)
{
    vec4 Result = B;
    Result.x = A / Result.x;
    Result.y = A / Result.y;
    Result.z = A / Result.z;
    Result.w = A / Result.w;
    return(Result);
}

inline void
operator+=(vec4 &A, f32 B)
{
    A = A + B;
}

inline void
operator-=(vec4 &A, f32 B)
{
    A = A - B;
}

inline void
operator*=(vec4 &A, f32 B)
{
    A = A * B;
}

inline void
operator/=(vec4 &A, f32 B)
{
    A = A / B;
}

inline vec4
operator+(vec4 A, vec4 B)
{
    vec4 Result;
    Result.x = A.x + B.x;
    Result.y = A.y + B.y;
    Result.z = A.z + B.z;
    Result.w = A.w + B.w;
    return(Result);
}

inline vec4
operator-(vec4 A, vec4 B)
{
    vec4 Result;
    Result.x = A.x - B.x;
    Result.y = A.y - B.y;
    Result.z = A.z - B.z;
    return(Result);
}

inline vec4
operator*(vec4 A, vec4 B)
{
    vec4 Result;
    Result.x = A.x * B.x;
    Result.y = A.y * B.y;
    Result.z = A.z * B.z;
    return(Result);
}

inline vec4
operator/(vec4 A, vec4 B)
{
    vec4 Result;
    Result.x = A.x / B.x;
    Result.y = A.y / B.y;
    Result.z = A.z / B.z;
    return(Result);
}

inline void
operator+=(vec4 &A, vec4 B)
{
    A = A + B;
}

inline void
operator-=(vec4 &A, vec4 B)
{
    A = A - B;
}

inline void
operator*=(vec4 &A, vec4 B)
{
    A = A * B;
}

inline void
operator/=(vec4 &A, vec4 B)
{
    A = A / B;
}

inline b32
operator==(vec4 A, vec4 B)
{
    b32 Result = false;
    if((A.x == B.x) &&
       (A.y == B.y) &&
       (A.z == B.z))
    {
        Result = true;
    }
    return(Result);
}

inline b32
operator!=(vec4 A, vec4 B)
{
    b32 Result = false;
    if((A.x != B.x) ||
       (A.y != B.y) ||
       (A.z != B.z))
    {
        Result = true;
    }
    return(Result);
}

inline f32
SquareRoot(f32 Value)
{
    __m128 InputValue = _mm_broadcast_ss(&Value);
    __m128 OutputSquaredValue = _mm_sqrt_ss(InputValue);
    f32 Result = _mm_cvtss_f32(OutputSquaredValue);
    return(Result);
}

function b32
IsVectorEqual(vec3 &A, vec3 &B)
{
    b32 Result = ((A.x == B.x) && (A.y == B.y) && (A.z == B.z));
    return(Result);
}

function b32
IsVectorEqual(vec4 &A, vec4 &B)
{
    b32 Result = ((A.x == B.x) && (A.y == B.y) && (A.z == B.z) && (A.w == B.w));
    return(Result);
}

function f32
VectorLength(vec3 V)
{
    f32 Result = SquareRoot(Square(V.x) + Square(V.y) + Square(V.z));
    return(Result);
}

function f32
VectorLength(vec4 V)
{
    f32 Result = SquareRoot(Square(V.x) + Square(V.y) + Square(V.z) + Square(V.w));
    return(Result);
}

function f32
Dot(vec3 A, vec3 B)
{
    f32 Result = (A.x * B.x) + (A.y * B.y) + (A.z * B.z);
    return(Result);
}

function f32
Dot(vec4 A, vec4 B)
{
    f32 Result = (A.x * B.x) + (A.y * B.y) + (A.z * B.z) + (A.w * B.w);
    return(Result);
}

function f32
DotSelf(vec3 A)
{
    return(Dot(A, A));
}

function f32
DotSelf(vec4 A)
{
    return(Dot(A, A));
}

function vec3
Cross(vec3 A, vec3 B)
{
    vec3 Result;
    Result.x = (A.y * B.z) - (A.z * B.y);
    Result.y = (A.z * B.x) - (A.x * B.z);
    Result.z = (A.x * B.y) - (A.y * B.x);
    return(Result);
}

function vec3
Normalize(vec3 Vector)
{
    f32 Length = VectorLength(Vector);
    
    Vector.x /= Length;
    Vector.y /= Length;
    Vector.z /= Length;
    
    return(Vector);
}

function vec4
Normalize(vec4 Vector)
{
    f32 Length = VectorLength(Vector);
    
    Vector.x /= Length;
    Vector.y /= Length;
    Vector.z /= Length;
    Vector.w /= Length;
    
    return(Vector);
}

function f32
MapToRange(f32 Value, f32 MinOld, f32 MaxOld, f32 MinNew, f32 MaxNew)
{
    f32 Result = ((Value - MinOld) / (MaxOld - MinOld)) * (MaxNew - MinNew) + MinNew;
    return(Result);
}

function void
SolveQuadratic(f32 A, f32 B, f32 C, f32 *Root1, f32 *Root2)
{
    f32 Res1 = 0.0f;
    f32 Res2 = 0.0f;
    
    if(A > 0.0001f)
    {
        f32 Delta = Square(B) - 4.0f * A * C;
        if(Delta == 0.0f)
        {
            Res1 = (-0.5f * B) / A;
        }
        else if(Delta > 0.0f)
        {
            f32 SignValue = B >= 0 ? 1.0f : -1.0f;
            f32 Q = -0.5f * (B + SignValue * SquareRoot(Delta));
            
            Res1 = Q / A;
            Res2 = C / Q;
        }
        
        f32 SwapTemp = 0.0f;
        if(Res1 > Res2)
        {
            SwapTemp = Res1;
            Res1 = Res2;
            Res2 = SwapTemp;
        }
    }
    
    *Root1 = Res1;
    *Root2 = Res2;
}

function f32
Max(f32 A, f32 B)
{
    return(A > B ? A : B);
}

// Returns a random real in [0,1).
function f32
RandomF32() 
{
    f32 Result = (f32)rand() / RAND_MAX;
    return(Result);
}

function vec3
Lerp(vec3 A, vec3 B, f32 T)
{
    f32 X = A.x + (B.x - A.x) * T;
    f32 Y = A.y + (B.y - A.y) * T;
    f32 Z = A.z + (B.z - A.z) * T;
    vec3 Result = V3(X, Y, Z);
    return(Result);
}

function vec4
Lerp(vec4 A, vec4 B, f32 T)
{
    f32 X = A.x + (B.x - A.x) * T;
    f32 Y = A.y + (B.y - A.y) * T;
    f32 Z = A.z + (B.z - A.z) * T;
    f32 W = A.w + (B.w - A.w) * T;
    vec4 Result = V4(X, Y, Z, W);
    return(Result);
}

function f32
Clamp(f32 X, f32 RangeLow, f32 RangeHigh)
{
    if(X < RangeLow)
    {
        X = RangeLow;
    }
    else if(X > RangeHigh)
    {
        X = RangeHigh;
    }
    return(X);
}

function vec3
Clamp(vec3 X, f32 RangeLow, f32 RangeHigh)
{
    if(X.x < RangeLow)
    {
        X.x = RangeLow;
    }
    if(X.y < RangeLow)
    {
        X.y = RangeLow;
    }
    if(X.z < RangeLow)
    {
        X.z = RangeLow;
    }
    
    if(X.x > RangeHigh)
    {
        X.x = RangeHigh;
    }
    if(X.y > RangeHigh)
    {
        X.y = RangeHigh;
    }
    if(X.z > RangeHigh)
    {
        X.z = RangeHigh;
    }
    return(X);
}

// Random value in normal distribution (with mean=0 and sd=1)
// https://stackoverflow.com/a/6178290
function f32
RandomNormalDistribution()
{
    f32 Theta = 2 * (f32)PI * RandomF32();
    f32 Rho = SquareRoot(-2 * logf(RandomF32()));
    return(Rho * cosf(Theta));
}

function f32
Sign(f32 Input)
{
    f32 Result = 0.0f;
    if(Input < 0.0f)
    {
        Result = -1.0f;
    }
    else
    {
        Result = 1.0f;
    }
    return(Result);
}