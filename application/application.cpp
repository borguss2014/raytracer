#include "application.h"

timer GlobalTimer = {};
global_variable raytracer RaytracerApp = {};

function u32
GetRenderTargetSize(render_target *Target)
{
    u32 Result = Target->Width * Target->Height;
    return(Result);
}

function void
WriteToRenderTarget(render_target *Target, void *PixelData, u32 PixelIndex, u32 PixelCount)
{
    Assert(Target);
    Assert(PixelData);

    pixel_format_info PixelFormatInfo = Target->FormatInfo;
    u32 Stride = PixelFormatInfo.ComponentByteSize * PixelFormatInfo.Components;
    u32 BufferOffset = PixelIndex * Stride;

    u8 *BufferDest = RaytracerApp.RenderOutputTarget.Buffer.Data + BufferOffset;
    u32 PixelDataSize = Stride * PixelCount;

    Platform.CopyMemory(BufferDest, PixelDataSize, PixelData, PixelDataSize);
}

function mmsz
GetBufferByteOffset(render_target *Src, u32 Idx)
{
    pixel_format_info PixelFormatInfo = Src->FormatInfo;
    u32 Stride = PixelFormatInfo.ComponentByteSize * PixelFormatInfo.Components;
    mmsz BufferOffset = Idx * Stride;
    return(BufferOffset);
}

function vec4
GetPixelDataAtIndex_VEC4(render_target *Src, u32 Idx)
{
    mmsz BufferOffset = GetBufferByteOffset(Src, Idx);
    vec4 Result = *(vec4 *)(Src->Buffer.Data + BufferOffset);
    return(Result);
}

function u32
GetPixelDataAtIndex_U32(render_target *Src, u32 Idx)
{
    mmsz BufferOffset = GetBufferByteOffset(Src, Idx);
    u32 Result = *(u32* )(Src->Buffer.Data + BufferOffset);
    return(Result);
}

function bitmap_image
CreateBitmapImage(u32 Width, u32 Height, memory_arena *Arena = &DefaultArena)
{
    bitmap_image Result = {};
    Result.Width = Width;
    Result.Height = Height;

    u32 TotalPixels = Width * Height;
    if(TotalPixels != 0)
    {
        Result.Header.Type = 0x4d42;
        Result.Header.OffsetBytes = sizeof(Result.Header);

        Result.Header.StructureSize = sizeof(Result.Header) - OffsetOf(bitmap_header, StructureSize);
        Result.Header.Width = Width;
        Result.Header.Height = Height;
        Result.Header.Planes = 1;
        Result.Header.BitsPerPixel = 64; // Results in ~60 megabytes for a 4K uncompressed image

        u32 BI_RGB = 0; // Uncompressed RGB
        Result.Header.Compression = BI_RGB;

        Result.Header.HorizontalPixelsPerMeter = 0;
        Result.Header.VerticalPixelsPerMeter = 0;
        Result.Header.ColorIndicesUsed = 0;
        Result.Header.ColorIndicesImportant = 0;

        u32 OutputImageSize = TotalPixels * Result.Header.BitsPerPixel;
        Result.Header.SizeImage = OutputImageSize;
        Result.Header.FileSize = sizeof(Result.Header) + OutputImageSize;

        Result.Data = PushByteBuffer(OutputImageSize);
    }
    
    return(Result);
}

function u16
DitherValue(f32 Input)
{
    /* [DITHERING]
        http://eastfarthing.com/blog/2015-12-19-color/
    */
    f32 Dither = RandomF32() - 0.5f;
    u16 Result = (u16)Clamp(Input * 256.0f + Dither, 0.0f, 255.0f);
    return(Result);
}

function void
CopyRenderTargetToImage(render_target *RenderTarget, bitmap_image *Image, b32 Dithering = true)
{
    u32 RenderTargetTotalPixels = RenderTarget->Width * RenderTarget->Height;
    Assert(RenderTargetTotalPixels == Image->Data.Capacity);

    for(u32 PixelIndex = 0; PixelIndex < RenderTargetTotalPixels; ++PixelIndex)
    {
        vec4 CurrentTargetPixel = GetPixelDataAtIndex_VEC4(RenderTarget, PixelIndex);

        image_pixel_data *CurrentImagePixel = (image_pixel_data *)Image->Data[PixelIndex * Image->Header.BitsPerPixel];
        CurrentImagePixel->B = Dithering ? DitherValue(CurrentTargetPixel.b) : (u16)CurrentTargetPixel.b;
        CurrentImagePixel->G = Dithering ? DitherValue(CurrentTargetPixel.g) : (u16)CurrentTargetPixel.g;
        CurrentImagePixel->R = Dithering ? DitherValue(CurrentTargetPixel.r) : (u16)CurrentTargetPixel.r;
        //CurrentImagePixel->A = (u16)CurrentTargetPixel.a;
        CurrentImagePixel->A = 255;
    }
}

function void
WriteImageToFile(bitmap_image *Image, render_target *OutputTarget, const char *Name)
{   
    u32 TotalPixels = GetRenderTargetSize(OutputTarget);
    for(u32 PixelIndex = 0; PixelIndex < TotalPixels; ++PixelIndex)
    {
        vec4 PixelColor = GetPixelDataAtIndex_VEC4(OutputTarget, PixelIndex);

        CopyRenderTargetToImage(OutputTarget, Image);
    }

    FILE *OutputBitmapHandle = fopen(Name, "wb");
    if(OutputBitmapHandle)
    {
        fwrite(&Image->Header, sizeof(Image->Header), 1, OutputBitmapHandle);
        fwrite(Image->Data, GetImageSize(Image), 1, OutputBitmapHandle);
        fclose(OutputBitmapHandle);
    }
}

function void
InitializeScene(world_storage *WorldStorage, memory_arena *Arena = &DefaultArena)
{

}

function hit_info
TraceRayInScene(vec4 RayOrigin, vec4 RayDirection, f32 MaxDistance, world_storage *WorldStorage)
{
    hit_info HitInfo = {};

    f32 SmallestDistanceAlongRay = F32Max;
    storage *SpheresStorage = GetStorageType(WorldStorage, StorageType_Sphere);
    storage *PlanesStorage = GetStorageType(WorldStorage, StorageType_Plane);
    
    /*
        NOTE(Cristian): Normally the magnitude of A should be 1.0f after computing 
        a dot product with itself, but the ray direction vector might not be normalized.
    */ 
    f32 A = DotSelf(RayDirection);

    storage_iterator StorageIterator = CreateStorageIterator(SpheresStorage);
    for(u32 SphereIndex = 0; SphereIndex < SpheresStorage->Used; ++SphereIndex)
    {
        sphere *CurrentSphere = GetNextStorageItem(&StorageIterator, sphere);
        
        vec4 SphereCenterToRayOrigin = RayOrigin - CurrentSphere->Center;
        
        f32 B = 2.0f * Dot(RayDirection, SphereCenterToRayOrigin);
        f32 C = DotSelf(SphereCenterToRayOrigin) - Square(CurrentSphere->Radius);
        
        f32 PIntersectRatio1, PIntersectRatio2;
        SolveQuadratic(A, B, C, &PIntersectRatio1, &PIntersectRatio2);
        
        f32 PIntersectRatio = PIntersectRatio2;
        if(PIntersectRatio1 > 0.0f)
        {
            PIntersectRatio = PIntersectRatio1;
        }
        
        if((PIntersectRatio > 0.0f) && (PIntersectRatio < SmallestDistanceAlongRay))
        {
            SmallestDistanceAlongRay = PIntersectRatio;
            
            HitInfo.EntityIndex = SphereIndex;
            HitInfo.EntityType = EntityType_Sphere;
            HitInfo.MaterialIndex = CurrentSphere->MaterialIndex;
        }
    }

    StorageIterator = CreateStorageIterator(PlanesStorage);
    for(u32 PlaneIndex = 0; PlaneIndex < PlanesStorage->Used; ++PlaneIndex)
    {
        plane *CurrentPlane = GetNextStorageItem(&StorageIterator, plane);
        
        f32 Denominator = Dot(CurrentPlane->Normal, RayDirection);
        if(fabs(Denominator) > 0.0001f)
        {
            f32 PIntersectionRatio = Dot((CurrentPlane->Origin - RayOrigin), CurrentPlane->Normal) / Denominator;
            if(PIntersectionRatio >= 0.0f)
            {
                if(PIntersectionRatio < SmallestDistanceAlongRay)
                {
                    SmallestDistanceAlongRay = PIntersectionRatio;
                    
                    HitInfo.EntityIndex = PlaneIndex;
                    HitInfo.EntityType = EntityType_Plane;
                    HitInfo.MaterialIndex = CurrentPlane->MaterialIndex;
                }
            }
        }
    }
    
    // TODO(Cristian): Redo the "entity" types
    if(HitInfo.EntityType != EntityType_Unknown)
    {
        HitInfo.HitPosition = RayOrigin + SmallestDistanceAlongRay * RayDirection;
        if(VectorLength(HitInfo.HitPosition - V4(0.0f)) <= MaxDistance)
        {
            HitInfo.MadeContact = true;

            switch(HitInfo.EntityType)
            {
                case EntityType_Plane:
                {
                    plane *HitPlane = GetFromStorageAtIndex(PlanesStorage, HitInfo.EntityIndex, plane);
                    HitInfo.HitSurfaceNormal = HitPlane->Normal;
                } break;
                
                case EntityType_Sphere:
                {
                    sphere *HitSphere = GetFromStorageAtIndex(SpheresStorage, HitInfo.EntityIndex, sphere);
                    HitInfo.HitSurfaceNormal = Normalize(HitInfo.HitPosition - HitSphere->Center);
                } break;
            }
        }
        else
        {
            HitInfo = {};
        }
    }
    
    return(HitInfo);
}

function vec3
GetLinearizedValue(vec3 Data, f32 Gamma)
{
    vec3 LinearizedResult = V3(powf(Data.x, Gamma),
                               powf(Data.y, Gamma),
                               powf(Data.z, Gamma));
    return(LinearizedResult);
}

function vec4
RandomDirection()
{
    vec4 Result = V4(RandomNormalDistribution(), 
                     RandomNormalDistribution(), 
                     RandomNormalDistribution(),
                     0.0f);
    return(Normalize(Result));
}

function vec4
RandomHemisphereDirection(vec4 Normal)
{
    vec4 Direction = RandomDirection();
    return(Direction * Sign(Dot(Normal, Direction)));
}

function b32
RayHitASurface(hit_info *Info)
{
    b32 Result = Info->MaterialIndex;
    return(Result);
}

function f32
GetLuminance(vec4 PixelColor)
{
    return(Dot(PixelColor, V4(V3(0.2126f, 0.7152f, 0.0722f))));
}

function f32
ExtractHighlights(render_target *SrcTarget, render_target *DestTarget, f32 Threshold = 1.0f)
{
    f32 HighestLuminance = 0.0f;

    u32 TotalPixels = GetRenderTargetSize(SrcTarget);
    for(u32 PixelIndex = 0; PixelIndex < TotalPixels; ++PixelIndex)
    {
        pixel_format_info PixelFormatInfo = SrcTarget->FormatInfo;
        u32 Stride = PixelFormatInfo.ComponentByteSize * PixelFormatInfo.Components;
        u32 BufferOffset = PixelIndex * Stride;

        // TODO(Cristian): Unfortunately I cannot figure out how to get type information in the current system... so hardcoding it is for now
        vec4 *CurrentPixelColor = (vec4 *)(SrcTarget->Buffer.Data + BufferOffset);

        f32 PixelBrightness = GetLuminance(*CurrentPixelColor);
        if(PixelBrightness >= Threshold)
        {
            // TODO(Cristian): Should I be able to copy data between render targets with different formats....?
            // Seems like something to explore, although I expect there to be some data loss involved in the process
            vec4 *DestPixelColor = (vec4 *)(DestTarget->Buffer.Data + BufferOffset);
            *DestPixelColor = *CurrentPixelColor;
        }

        // NOTE(Cristian): Storing the highest luminance even if it doesn't pass the threshold test
        if(PixelBrightness > HighestLuminance)
        {
            HighestLuminance = PixelBrightness;
        }
    }

    return(HighestLuminance);
}

function void
GaussianBlur(render_target *OutputTarget, gaussianblur_type BlurType, f32 *WeightsData, u32 WeightsCount)
{
    u32 PixelIndex = 0;
    for(u32 Y = 0; Y < OutputTarget->Height; ++Y)
    {
        for(u32 X = 0; X < OutputTarget->Width; ++X)
        {
            vec4 CenterPixelColor = GetPixelDataAtIndex_VEC4(OutputTarget, PixelIndex) * WeightsData[0];

            for(u32 WeightIndex = 1; WeightIndex < WeightsCount; ++WeightIndex)
            {   
                vec4 PreviousPixelColor = V4(0.0f);
                vec4 NextPixelColor = V4(0.0f);
                if(BlurType == GaussianBlur_Horizontal)
                {
                    if((X - WeightIndex) >= 0)
                    {
                        PreviousPixelColor = GetPixelDataAtIndex_VEC4(OutputTarget, PixelIndex - WeightIndex) * WeightsData[WeightIndex];
                    }

                    if((X + WeightIndex) <= (OutputTarget->Width - 1))
                    {
                        NextPixelColor = GetPixelDataAtIndex_VEC4(OutputTarget, PixelIndex + WeightIndex) * WeightsData[WeightIndex];
                    }
                }
                else if(BlurType == GaussianBlur_Vertical)
                {
                    // Vertical blur
                    if((Y - WeightIndex) >= 0)
                    {
                        PreviousPixelColor = GetPixelDataAtIndex_VEC4(OutputTarget, PixelIndex - OutputTarget->Width * WeightIndex) * WeightsData[WeightIndex];
                    }

                    if((Y + WeightIndex) <= (OutputTarget->Height - 1))
                    {
                        NextPixelColor = GetPixelDataAtIndex_VEC4(OutputTarget, PixelIndex + OutputTarget->Width * WeightIndex) * WeightsData[WeightIndex];
                    }
                }

                CenterPixelColor += PreviousPixelColor + NextPixelColor;
            }

            WriteToRenderTarget(OutputTarget, &CenterPixelColor, PixelIndex, 1);

            PixelIndex++;
        }
    }
}

// Collapses HDR values into LDR/SDR
function vec4
ApplyReinhardExtendedLuminance(vec4 PixelColor, f32 MaxWhiteLight = 1.0f)
{
    f32 OldLuminance = GetLuminance(PixelColor);
    f32 Numerator = OldLuminance * (1.0f + (OldLuminance / (Square(MaxWhiteLight))));
    f32 NewLuminance = Numerator / (1.0f + OldLuminance);
    return(PixelColor * (NewLuminance / OldLuminance));
}

enum tonemapping_type
{
    TonemapType_Reinhard_None = 0,

    TonemapType_Reinhard_Standard,
    TonemapType_Reinhard_ExtendedLuminance
};

struct hdr_to_ldr_config
{
    tonemapping_type TonemapType;
    f32 Gamma;
    f32 MaxWhitePoint;
};

function hdr_to_ldr_config
DefaultHDRToLDRConfig()
{
    hdr_to_ldr_config Result = {};
    Result.Gamma = 2.2f;
    Result.MaxWhitePoint = 1.0;
    // Result.TonemapType = ; No tonemap by default
    return(Result);
}

function void
HDRToLDR(render_target *OutputTarget, hdr_to_ldr_config Config = DefaultHDRToLDRConfig())
{
    u32 TotalPixels = GetRenderTargetSize(OutputTarget);
    for(u32 PixelIndex = 0; PixelIndex < TotalPixels; ++PixelIndex)
    {
        // TODO(Cristian): NEED BETTER TYPE INFORMATION SYSTEM HERE!!!!!
        vec4 PixelColor = GetPixelDataAtIndex_VEC4(OutputTarget, PixelIndex);

        switch(Config.TonemapType)
        {
            case TonemapType_Reinhard_Standard:
            {
                PixelColor /= (PixelColor + V4(1.0f));
            } break;

            case TonemapType_Reinhard_ExtendedLuminance:
            {
                PixelColor = ApplyReinhardExtendedLuminance(PixelColor, Config.MaxWhitePoint);
            } break;
        }

        f32 Gamma = Config.Gamma;

        // Gamma correction
        PixelColor = V4(powf(PixelColor.r, 1.0f / Gamma),
                        powf(PixelColor.g, 1.0f / Gamma),
                        powf(PixelColor.b, 1.0f / Gamma),
                        0.0f);

        WriteToRenderTarget(OutputTarget, &PixelColor, PixelIndex, 1);
    }
}

#include <iostream> // TODO(Cristian): Remove this in the future

JOB_QUEUE_ENTRY_CALLBACK(ProcessPixel)
{
    memory_arena ThreadLocalStorage = {};

    job_data *DataToProcess = (job_data *)Payload.Data;

    camera *Camera = &RaytracerApp.Camera;
    viewport *Viewport = &RaytracerApp.Camera.Viewport;

    vec4 PixelColorAccumulator = V4(0.0f);

    timer ThreadTimer = {};
    StartTimer(&ThreadTimer);

    for(u32 RayIndex = 0; RayIndex < RaytracerApp.RaysPerPixel; ++RayIndex)
    {
        f32 DisplacementX = (RandomF32() - 0.5f) * RaytracerApp.PixelInfo.HalfWidth;
        f32 DisplacementY = (RandomF32() - 0.5f) * RaytracerApp.PixelInfo.HalfHeight;

        // NOTE(Cristian): ViewportCenter is in worldspace coordinates
        vec4 ViewportCenter = Viewport->ViewportCenter;
        ViewportCenter.x += (DataToProcess->ViewportX + DisplacementX) * Viewport->HalfWidth;
        ViewportCenter.y += (DataToProcess->ViewportY + DisplacementY) * Viewport->HalfHeight;

        vec4 RayOrigin = Camera->Position;
        vec4 RayDirection = Normalize(ViewportCenter - Camera->Position);

        vec4 IncomingLight = V4(0.0f);
        vec4 RayColor = V4(1.0f);
        for(u32 RayBounceIndex = 0; RayBounceIndex < RaytracerApp.BouncesPerRay; ++RayBounceIndex)
        {
            hit_info HitData = TraceRayInScene(RayOrigin, RayDirection, Camera->FarPlane, &RaytracerApp.WorldStorage);
            if(!HitData.MadeContact)
            {
                break;
            }

            storage *MaterialsStorage = GetStorageType(&RaytracerApp.WorldStorage, StorageType_Material);
            material *Material = GetFromStorageAtIndex(MaterialsStorage, HitData.MaterialIndex, material);
            if(Material)
            {
                vec4 EmittedLight = Material->Emissive * Material->EmissiveStrength;
                IncomingLight += EmittedLight * RayColor;

                RayColor *= V4(GetLinearizedValue(V3(Material->Albedo), RaytracerApp.Camera.Gamma), 0.0f);
                if(RayColor == V4(0.0f))
                {
                    IncomingLight -= EmittedLight * RayColor;
                    break;
                }
                
                vec4 DiffuseDir = RandomHemisphereDirection(HitData.HitSurfaceNormal);
                vec4 SpecularDir = RayDirection - 2.0f * Dot(RayDirection, HitData.HitSurfaceNormal) * HitData.HitSurfaceNormal;
                RayDirection = Lerp(DiffuseDir, SpecularDir, Material->Roughness);
                RayOrigin = HitData.HitPosition + 0.0005f * RayDirection;
            }
        }

        PixelColorAccumulator += (IncomingLight);
    }

    // Average the color values of all rays shot into a pixel
    PixelColorAccumulator /= (f32)RaytracerApp.RaysPerPixel;

    // Average the color values over multiple frames
    PixelColorAccumulator /= (f32)RaytracerApp.TotalFrames;

    // TODO(Cristian): Setup render outputs for raytracer and bitmap output
    //RaytracerApp.RenderOutputTarget.Data[DataToProcess->PixelIndex] += (PixelColorAccumulator / (f32)RaytracerApp.TotalFrames);

    WriteToRenderTarget(&RaytracerApp.RenderOutputTarget, &PixelColorAccumulator, DataToProcess->PixelIndex, 1);

    StopTimer(&ThreadTimer);

    TicketMutexLock(&RaytracerApp.GlobalMutex);
    {
        RaytracerApp.Statistics.TotalPixelsTraced++;
        RaytracerApp.Statistics.AverageTimePerPixel_MS += GetTimeInMilliseconds(&ThreadTimer);

        u32 NewProgress = (RaytracerApp.Statistics.TotalPixelsTraced * 100) / RaytracerApp.TotalPixelsToTrace;
        if(NewProgress != RaytracerApp.Statistics.CurrentRaytraceProgress)
        {
            RaytracerApp.Statistics.CurrentRaytraceProgress = NewProgress;
            printf("-");
        }
    }
    TicketMutexUnlock(&RaytracerApp.GlobalMutex);
}

extern "C"
APP_INIT(Init)
{
    Platform = PlatformMemory->PlatformAPI;
    InitializeArena(&DefaultArena);

    RaytracerApp.RaysPerPixel = 16;
    RaytracerApp.BouncesPerRay = 4;
    RaytracerApp.TotalFrames = 1;
    RaytracerApp.RenderWidth = 1920;
    RaytracerApp.RenderHeight = 1080;
    RaytracerApp.TotalPixelsToTrace = RaytracerApp.RenderWidth * RaytracerApp.RenderHeight;
    
    RaytracerApp.PixelInfo.Width = 1.0f / RaytracerApp.RenderWidth;
    RaytracerApp.PixelInfo.Height = 1.0f / RaytracerApp.RenderHeight;
    RaytracerApp.PixelInfo.HalfWidth = RaytracerApp.PixelInfo.Width * 0.5f;
    RaytracerApp.PixelInfo.HalfHeight = RaytracerApp.PixelInfo.Height * 0.5f;
    
    RaytracerApp.RenderOutputTarget = CreateRenderTarget(RaytracerApp.RenderWidth, RaytracerApp.RenderHeight, PixelFormat_RGBA32);
    RaytracerApp.BloomOutputTarget = CreateRenderTarget(RaytracerApp.RenderWidth, RaytracerApp.RenderHeight, PixelFormat_RGBA32);

    RaytracerApp.WorldStorage = CreateWorldStorage();
    Platform.CreateJobQueue(&RaytracerApp.ProcessingQueue);

    RaytracerApp.Camera.Gamma = 2.2f;
    RaytracerApp.Camera.NearPlane = 1.0f;
    RaytracerApp.Camera.FarPlane = 1000.0f;
    RaytracerApp.Camera.AspectRatio = (f32)RaytracerApp.RenderWidth / (f32)RaytracerApp.RenderHeight;
    RaytracerApp.Camera.Viewport.Width = 1.0f;
    RaytracerApp.Camera.Viewport.Height = 1.0f;
    RaytracerApp.Camera.Viewport.HalfWidth = 0.5f * RaytracerApp.Camera.Viewport.Width;
    RaytracerApp.Camera.Viewport.HalfHeight = 0.5f * RaytracerApp.Camera.Viewport.Height;
    RaytracerApp.Camera.Viewport.ViewportCenter = RaytracerApp.Camera.Position - RaytracerApp.Camera.NearPlane * RaytracerApp.Camera.CameraZ;

    RaytracerApp.Camera.Position = V4(0.0f, 4.0f, 22.0f, 0.0f);
    RaytracerApp.Camera.CameraZ = Normalize(RaytracerApp.Camera.Position - RaytracerApp.Camera.Target);
    RaytracerApp.Camera.CameraX = V4(Normalize(Cross(V3(0.0f, 1.0f, 0.0f), V3(RaytracerApp.Camera.CameraZ))), 0.0f);
    RaytracerApp.Camera.CameraY = V4(Normalize(Cross(V3(RaytracerApp.Camera.CameraZ), V3(RaytracerApp.Camera.CameraX))), 0.0f);

    InitializeScene(&RaytracerApp.WorldStorage);
}

extern "C"
APP_UPDATE_AND_RENDER(UpdateAndRender)
{
    if(RaytracerApp.CurrentFrame < RaytracerApp.TotalFrames)
    {
        memory_arena FrameArena = {};
        InitializeArena(&FrameArena);

        job_data *DataToProcess = PushArray(&FrameArena, RaytracerApp.RenderWidth * RaytracerApp.RenderHeight, job_data);
        if(DataToProcess)
        {
            u32 PixelIndex = 0;
            viewport *Viewport = &RaytracerApp.Camera.Viewport;

            for(u32 Y = 0; Y < RaytracerApp.RenderHeight; ++Y)
            {
                f32 ViewportY = MapToRange((f32)Y, 0, (f32)RaytracerApp.RenderHeight, -Viewport->HalfHeight, Viewport->HalfHeight);
                
                for(u32 X = 0; X < RaytracerApp.RenderWidth; ++X)
                {
                    f32 ViewportX = MapToRange((f32)X, 0, (f32)RaytracerApp.RenderWidth, -Viewport->HalfWidth, Viewport->HalfWidth);
                    ViewportX *= RaytracerApp.Camera.AspectRatio;

                    { // NOTE(Cristian): Process jobs on the main thread if the job queue is full
                        if(RaytracerApp.ProcessingQueue.AvailableEntriesCount == RaytracerApp.ProcessingQueue.JobEntriesCapacity)
                        {
                            timer StallTimer = {};
                            while(RaytracerApp.ProcessingQueue.AvailableEntriesCount > RaytracerApp.ProcessingQueue.ThreadCount + 1)
                            {
                                StartTimer(&StallTimer);
                                Platform.DoNextAvailableJobEntry(&RaytracerApp.ProcessingQueue, MAIN_THREAD);
                            }
                            if(StallTimer.IsRunning)
                            {
                                StopTimer(&StallTimer);

                                f32 StallTimeMS = GetTimeInMilliseconds(&StallTimer);
                                printf("Main thread stalled for %fms\n", StallTimeMS);
                            }
                        }
                    }

                    job_data *CurrentDataToProcess = &DataToProcess[PixelIndex];
                    CurrentDataToProcess->ViewportX = ViewportX;
                    CurrentDataToProcess->ViewportY = ViewportY;
                    CurrentDataToProcess->PixelIndex = PixelIndex;

                    payload Payload = {};
                    // Payload.Offset = PixelIndex;
                    // Payload.BatchCount = 8;
                    Payload.Data = CurrentDataToProcess;

                    // TODO(Cristian): Queueing jobs seems to take a while, especially for higher resolution renders.
                    // Should look into mitigating this in the future.
                    // TODO(Cristian): Experiment with giving each thread more pixels to process in order to 
                    // skip all the overhead of retrieving jobs (and possibly avoiding the mutex lock as much as possible)
                    Platform.AddJobEntry(&RaytracerApp.ProcessingQueue, ProcessPixel, Payload, MAIN_THREAD);

                    PixelIndex++;
                }
            }
        }

        Platform.CompleteAllJobQueueWork(&RaytracerApp.ProcessingQueue, MAIN_THREAD);

        ClearArena(&FrameArena);

        RaytracerApp.CurrentFrame++;

        f32 Weights[5] = { 0.227027f, 0.1945946f, 0.1216216f, 0.054054f, 0.016216f };
        u32 TimesToBlur = 5;
        for(u32 Idx = 0; Idx < TimesToBlur; ++Idx)
        {
            f32 CurrentTime = GetElapsedTimeInMilliseconds(&GlobalTimer);
            if(CurrentTime >= 1000)
            {
                //printf(".");
                ResetTimer(&GlobalTimer);
            }

            GaussianBlur(&RaytracerApp.BloomOutputTarget, GaussianBlur_Vertical, Weights, ArrayCapacity(Weights));
            GaussianBlur(&RaytracerApp.BloomOutputTarget, GaussianBlur_Horizontal, Weights, ArrayCapacity(Weights));
        }

        GaussianBlur(&RaytracerApp.RenderOutputTarget, GaussianBlur_Vertical, Weights, ArrayCapacity(Weights));
        GaussianBlur(&RaytracerApp.RenderOutputTarget, GaussianBlur_Horizontal, Weights, ArrayCapacity(Weights));

        {
            hdr_to_ldr_config HDRToLDRConfig = {};
            HDRToLDRConfig.TonemapType = TonemapType_Reinhard_ExtendedLuminance;
            HDRToLDRConfig.MaxWhitePoint = ExtractHighlights(&RaytracerApp.RenderOutputTarget, &RaytracerApp.BloomOutputTarget);

            HDRToLDR(&RaytracerApp.RenderOutputTarget, HDRToLDRConfig);
            HDRToLDR(&RaytracerApp.BloomOutputTarget, HDRToLDRConfig);
        }
    }
    else
    {
        PlatformMemory->ApplicationRunning = false;
    }
}

extern "C"
APP_SHUTDOWN(Shutdown)
{
    
}