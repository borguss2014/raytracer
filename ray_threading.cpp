#define JOB_QUEUE_ENTRY_CALLBACK(name) void name(void *Data, u32 ThreadIndex)
typedef JOB_QUEUE_ENTRY_CALLBACK(job_queue_callback);

struct job_queue_entry
{
    job_queue_callback *Callback;
    void *Data;
};

struct job_queue
{
    //HANDLE SemaphoreHandle;
    void *SemaphoreHandle;
    
    u32 volatile AvailableEntriesCount;
    
    // TODO(Cristian): Implement a circular FIFO queue
    u32 volatile NextJobEntryToRead;
    u32 volatile NextJobEntryToWrite;
    
    job_queue_entry JobEntries[512];
    thread_info *ThreadInfos;
};

struct thread_info
{
    job_queue *Queue;
    DWORD LogicalThreadIndex;
};

function b32
DoNextAvailableJobEntry(job_queue *Queue, DWORD LogicalThreadIndex)
{
    b32 DidWork = false;
    
    u32 OriginalNextEntryToRead = Queue->NextJobEntryToRead;
    if (OriginalNextEntryToRead != Queue->NextJobEntryToWrite)
    {
        u32 EntryIndex = _InterlockedCompareExchange((LONG volatile *)&Queue->NextJobEntryToRead, 
                                                     (OriginalNextEntryToRead + 1) % ArrayCount(Queue->JobEntries), 
                                                     OriginalNextEntryToRead);
        if (EntryIndex == OriginalNextEntryToRead)
        {
            job_queue_entry JobEntry = Queue->JobEntries[EntryIndex];
            JobEntry.Callback(JobEntry.Data, LogicalThreadIndex);
            
            _InterlockedDecrement((LONG volatile *)&Queue->AvailableEntriesCount);
            
            DidWork = true;
        }
    }
    
    return(DidWork);
}

function DWORD WINAPI
ThreadProc(void *lpParameter)
{
    thread_info *ThreadInfo = (thread_info *)lpParameter;
    job_queue *Queue = ThreadInfo->Queue;
    
    for (;;)
    {
        b32 DidWork = DoNextAvailableJobEntry(Queue, ThreadInfo->LogicalThreadIndex);
        if (!DidWork)
        {
            WaitForSingleObjectEx(Queue->SemaphoreHandle, INFINITE, FALSE);
        }
    }
    
    return(0);
}

function void
MakeJobQueue(job_queue *Queue, thread_info *ThreadInfos,  u32 ThreadCount)
{
    Queue->ThreadInfos = (thread_info *)malloc(sizeof(thread_info) * NumberOfThreads);
    u32 InitialCount = 0;
    HANDLE SemaphoreHandle = CreateSemaphoreEx(0,
                                               InitialCount,
                                               ThreadCount,
                                               0, 0, SEMAPHORE_ALL_ACCESS);
    Queue->SemaphoreHandle = SemaphoreHandle;
    
    for (u32 ThreadIndex = 0;
         ThreadIndex < ThreadCount;
         ++ThreadIndex)
    {
        thread_info *ThreadInfo = ThreadInfos + ThreadIndex;
        ThreadInfo->LogicalThreadIndex = ThreadIndex + 1;
        ThreadInfo->Queue = Queue;
        
        DWORD ThreadID;
        HANDLE ThreadHandle = CreateThread(0, 0, ThreadProc, ThreadInfo, 0, &ThreadID);
        if (ThreadHandle)
        {
            CloseHandle(ThreadHandle);
        }
    }
}

function void
AddJobEntry(job_queue *Queue, job_queue_callback *Callback, void *Data, u32 ThreadIndex)
{
    u32 NewNextJobEntryToWrite = (Queue->NextJobEntryToWrite + 1) % ArrayCount(Queue->JobEntries);
    if(NewNextJobEntryToWrite != Queue->NextJobEntryToRead)
    {
        job_queue_entry *JobEntry = Queue->JobEntries + Queue->NextJobEntryToWrite;
        JobEntry->Callback = Callback;
        JobEntry->Data = Data;
        
        _InterlockedIncrement((LONG volatile *)&Queue->AvailableEntriesCount);
        
        Queue->NextJobEntryToWrite = NewNextJobEntryToWrite;
        
        ReleaseSemaphore(Queue->SemaphoreHandle, 1, 0);
    }
    else
    {
        printf("Job queue full! Missed tasks! \n");
    }
}

function void
CompleteAllWork(job_queue *Queue, u32 ThreadIndex)
{
    while(Queue->AvailableEntriesCount)
    {
        DoNextAvailableJobEntry(Queue, ThreadIndex);
    }
}