#pragma once

#include <dlfcn.h>
#include <sys/syslimits.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>

#include "../application/platform_shared.h"

extern void EntryPoint();

extern "C" PLATFORM_COPY_FILE(_macos_CopyFile);
extern "C" PLATFORM_CHECK_IF_FILE_EXISTS(_macos_CheckIfFileExists);
extern "C" PLATFORM_GET_FILE_LAST_WRITE_TIME(_macos_GetFileLastWriteTime);
extern "C" FILE * _macos_OpenFile(const char *Path, const char *Flag);
extern "C" mmsz _macos_GetFileSize(FILE *FileHandle);
extern "C" b32 _macos_ReadFile(FILE *FileHandle, void *Dest, s64 Offset, u32 BytesToRead, b32 ReadFromCurrentPosition = false);
extern "C" void _macos_RewindFileCursor(FILE *FileHandle);
extern "C" b32 _macos_CloseFile(FILE *FileHandle);

extern "C" s32 _macos_GetExePath(const char *Dst, s32 DstSize);

struct macOS_state
{
    char EXEPath[PATH_MAX];
    char *OnePastLastEXEPathSlash;

    b32 ApplicationRunning;
};

struct macOS_dynamic_code
{
    void *Dylib;
    
    char *SourceDylibPath;
    char *LockFilePath;
    const char *TransientDylibName;
    
    u32 TempDylibNumber;
    
    f64 LastDylibWriteTime;
    
    u32 FunctionsCount;
    const char **FunctionNames;
    void **Functions;
    
    b32 IsValid;
};

global_variable const char *MacOSApplicationFunctionTableNames[] =
{
    "Init",
    "UpdateAndRender",
    "Shutdown",
};

struct macOS_application_function_table
{
    app_init *Init;
    app_update_and_render *UpdateAndRender;
    app_shutdown *Shutdown;
};