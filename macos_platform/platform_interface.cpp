#include "platform_interface.h"

function b32
MacOSFileTimeIsValid(f64 Time)
{
    return(Time != 0);
}

function timestamps_comparison
MacOSCompareTimestamps(f64 First, f64 Second)
{
    timestamps_comparison Result = Timestamps_Equal;
    Result = (First < Second) ? Timestamps_FirstIsEarlier : Timestamps_FirstIsLater;

    return(Result);
}

function void
MacOSUnloadCode(macOS_dynamic_code *DynamicCode)
{
    if(DynamicCode->Dylib)
    {
        // TODO(Cristian): This might cause problems in the future with pointers to strings inside the Dylib.
        dlclose(DynamicCode->Dylib);
    }
    
    DynamicCode->Dylib = 0;
    DynamicCode->IsValid = false;
    
    for(u32 FunctionIndex = 0;
        FunctionIndex < DynamicCode->FunctionsCount;
        ++FunctionIndex)
    {
        DynamicCode->Functions[FunctionIndex] = 0;
    }
}

function void
MacOSConcatEXEPath(macOS_state *State, const char *DLLName, char *Dest, memory_size DestSize)
{
    char PathToEXE[PATH_MAX];
    memcpy(PathToEXE, State->EXEPath, State->OnePastLastEXEPathSlash - State->EXEPath);
    *(PathToEXE + (State->OnePastLastEXEPathSlash - State->EXEPath)) = 0;
    
    snprintf(Dest, DestSize, "%s%s", PathToEXE, DLLName);
}

function void
MacOSConcatEXEPath(macOS_state *State, const char *DLLName, u32 UniqueTag, char *Dest, memory_size DestSize)
{
    char PathToEXE[PATH_MAX];
    memcpy(PathToEXE, State->EXEPath, State->OnePastLastEXEPathSlash - State->EXEPath);
    *(PathToEXE + (State->OnePastLastEXEPathSlash - State->EXEPath)) = 0;
    
    snprintf(Dest, DestSize, "%s%d_%s", PathToEXE, UniqueTag, DLLName);
}

function void
MacOSLoadCode(macOS_state *State, macOS_dynamic_code *DynamicCode)
{
    char *SourceDylibPath = DynamicCode->SourceDylibPath;

    if(!_macos_CheckIfFileExists(DynamicCode->LockFilePath) &&
        _macos_CheckIfFileExists(SourceDylibPath))
    {
        DynamicCode->LastDylibWriteTime = _macos_GetFileLastWriteTime(SourceDylibPath);
        DynamicCode->Dylib = dlopen("application.dylib", RTLD_LAZY);
        if (DynamicCode->Dylib)
        {
            DynamicCode->IsValid = true;
            
            for(u32 FunctionIndex = 0;
                FunctionIndex < DynamicCode->FunctionsCount;
                ++FunctionIndex)
            {
                void *Function = dlsym(DynamicCode->Dylib, DynamicCode->FunctionNames[FunctionIndex]);
                if(Function)
                {
                    DynamicCode->Functions[FunctionIndex] = Function;
                }
                else
                {
                    // TODO(Cristian): Could maybe find a way to only use valid functions.. ?
                    // Maybe only fail if core functionality is not loaded?
                    DynamicCode->IsValid = false;
                    break;
                }
            }
        }

        if(!DynamicCode->IsValid)
        {
            MacOSUnloadCode(DynamicCode);
        }
    }
}

function void
MacOSReloadCode(macOS_state *State, macOS_dynamic_code *DynamicCode)
{
    if(!_macos_CheckIfFileExists(DynamicCode->LockFilePath))
    {
        f64 CurrentDylibWriteTime = _macos_GetFileLastWriteTime(DynamicCode->SourceDylibPath);
        if(MacOSFileTimeIsValid(DynamicCode->LastDylibWriteTime) && MacOSFileTimeIsValid(CurrentDylibWriteTime))
        {
            timestamps_comparison Comparison = MacOSCompareTimestamps(DynamicCode->LastDylibWriteTime, CurrentDylibWriteTime);
            b32 DylibNeedsToReload = (Comparison == Timestamps_FirstIsEarlier);
            if(DylibNeedsToReload)
            {
                MacOSUnloadCode(DynamicCode);
                MacOSLoadCode(State, DynamicCode);
            }
        }
    }
}

function void
MacOSGetEXEPath(macOS_state *State)
{
    u32 MainEXEFullPathSize = _macos_GetExePath(State->EXEPath, sizeof(State->EXEPath));
    State->OnePastLastEXEPathSlash = State->EXEPath;
    for(char *At = State->EXEPath;
        *At;
        ++At)
    {
        if(*At == '/')
        {
            State->OnePastLastEXEPathSlash = At + 1;
        }
    }
}

// TODO(Cristian): Maybe should have a shared definition of a memory block... ?
struct macOS_memory_block
{
    platform_memory_block Block;
    macOS_memory_block *Prev;
    macOS_memory_block *Next;
};

function
PLATFORM_ALLOCATE_MEMORY(MacOSAllocateMemory)
{
    memory_size TotalSize = Size + sizeof(macOS_memory_block);
    memory_size BlockOffset = sizeof(macOS_memory_block);
    memory_size ProtectOffset = 0;
    
    memory_size PageSize = (memory_size)getpagesize();
    if(AllocationFlags & PlatformFlags_UnderflowCheck)
    {
        TotalSize = 2 * PageSize + Size;
        BlockOffset = 2 * PageSize;
        ProtectOffset = PageSize;
    }
    else if(AllocationFlags & PlatformFlags_OverflowCheck)
    {
        memory_size PageAlignedSize = AlignToPow2(Size, PageSize);
        TotalSize = PageSize + PageAlignedSize + PageSize;
        BlockOffset = PageSize + PageAlignedSize - Size;
        ProtectOffset = PageSize + PageAlignedSize;
    }

    /*
        If addr is zero, an address will be selected by
        the system.  The actual starting address of the region is returned.
    */
    macOS_memory_block *NewBlock = (macOS_memory_block *)mmap(0, TotalSize, PROT_READ | PROT_WRITE, MAP_ANON | MAP_SHARED, 0, 0);
    Assert(NewBlock);

    NewBlock->Block.Base = (ump)NewBlock + BlockOffset;
    
    Assert(NewBlock->Block.Used == 0);
    Assert(NewBlock->Block.PrevBlock == 0);
    
    if(AllocationFlags & (PlatformFlags_UnderflowCheck | PlatformFlags_OverflowCheck))
    {
        b32 ProtectSuccess = mprotect((void *)((ump)NewBlock + ProtectOffset), PageSize, PROT_NONE);
        Assert(ProtectSuccess == 0);
    }
    
    macOS_memory_block *Sentinel = &GlobalWin32State.MemorySentinel;
    NewBlock->Next = Sentinel;
    NewBlock->Block.Size = Size;
    NewBlock->Block.Flags = AllocationFlags;
    
    TicketMutexLock(&GlobalWin32State.MemoryMutex);
    NewBlock->Prev = Sentinel->Prev;
    NewBlock->Prev->Next = NewBlock;
    NewBlock->Next->Prev = NewBlock;
    TicketMutexUnlock(&GlobalWin32State.MemoryMutex);
    
    platform_memory_block *PlatformBlock = &NewBlock->Block;
    return(PlatformBlock);
}

function void
MacOSFreeMemoryBlock(macOS_memory_block *Block)
{
    TicketMutexLock(&GlobalWin32State.MemoryMutex);
    Block->Prev->Next = Block->Next;
    Block->Next->Prev = Block->Prev;
    TicketMutexUnlock(&GlobalWin32State.MemoryMutex);
    
    b32 Result = VirtualFree(Block, 0, MEM_RELEASE);
    munmap(Block, );
    Assert(Result);
}

function
PLATFORM_DEALLOCATE_MEMORY(MacOSDeallocateMemory)
{
    macOS_memory_block *Block = (macOS_memory_block *)PlatformBlock;
    if(Block)
    {
        Win32FreeMemoryBlock(Block);
    }
}

function void
MacOSInitializePlatformAPI()
{
    platform_api PlatformAPI = {};
    PlatformAPI.CopyFile                    = _macos_CopyFile;
    PlatformAPI.CheckIfFileExists           = _macos_CheckIfFileExists;
    PlatformAPI.GetFileLastWriteTime        = _macos_GetFileLastWriteTime;
    PlatformAPI.AllocateMemory              = MacOSAllocateMemory;
    PlatformAPI.DeallocateMemory            = MacOSDeallocateMemory;

    PlatformAPI.AtomicExchangeU32           = MacOSAtomicExchangeU32;
    PlatformAPI.AtomicAddU32                = MacOSAtomicAddU32;
    PlatformAPI.AtomicCompareExchangeU32    = MacOSAtomicCompareExchangeU32;
    PlatformAPI.AtomicIncrementU32          = MacOSAtomicIncrementU32;
    PlatformAPI.AtomicDecrementU32          = MacOSAtomicDecrementU32;

    PlatformAPI.AtomicExchangeU64           = MacOSAtomicExchangeU64;
    PlatformAPI.AtomicAddU64                = MacOSAtomicAddU64;
    PlatformAPI.AtomicCompareExchangeU64    = MacOSAtomicCompareExchangeU64;
    PlatformAPI.AtomicIncrementU64          = MacOSAtomicIncrementU64;
    PlatformAPI.AtomicDecrementU64          = MacOSAtomicDecrementU64;

    PlatformAPI.CreateSemaphore             = MacOSCreateSemaphore;
    PlatformAPI.SemaphoreWait               = MacOSWaitForSingleObject;
    PlatformAPI.GetLogicalProcessorCount    = MacOSGetLogicalProcessorCount;

    PlatformAPI.CreateThread                = MacOSCreateThread;

    Platform = PlatformAPI;
}

void
EntryPoint()
{
    macOS_state State = {};
    State.ApplicationRunning = true;
    MacOSGetEXEPath(&State);

    char ApplicationDylibFullPath[PATH_MAX];
    MacOSConcatEXEPath(&State, "application.dylib", ApplicationDylibFullPath, sizeof(ApplicationDylibFullPath));

    char LockFileFullPath[PATH_MAX];
    MacOSConcatEXEPath(&State, "lock.tmp", LockFileFullPath, sizeof(LockFileFullPath));

    macOS_application_function_table ApplicationFunctions = {};
    macOS_dynamic_code ApplicationCode = {};
    ApplicationCode.SourceDylibPath = ApplicationDylibFullPath;
    ApplicationCode.TransientDylibName = "application_temp.dylib";
    ApplicationCode.LockFilePath = LockFileFullPath;
    ApplicationCode.FunctionsCount = ArrayCount(MacOSApplicationFunctionTableNames);
    ApplicationCode.FunctionNames = MacOSApplicationFunctionTableNames;
    ApplicationCode.Functions = (void **)&ApplicationFunctions;
    MacOSLoadCode(&State, &ApplicationCode);

    if(ApplicationFunctions.Init)
    {
        ApplicationFunctions.Init();
    }

    while(State.ApplicationRunning)
    {
        MacOSReloadCode(&State, &ApplicationCode);

        if(ApplicationFunctions.UpdateAndRender)
        {
            ApplicationFunctions.UpdateAndRender(&ApplicationMemory, 0);
        }
    }

    if(ApplicationFunctions.Shutdown)
    {
        ApplicationFunctions.Shutdown();
    }
}