import Foundation

// GLOBALS
let OSFileManager: FileManager = FileManager.default

extension Date {
    func timeInMillis() -> Double {
        return Double(self.timeIntervalSince1970 * 1000)
    }
}
// *************************************

// MACOS PLATFORM FUNCTIONALITY
@_cdecl("_macos_OpenFile")
func _macos_OpenFile(SourceCPath: UnsafePointer<CChar>, Flag: UnsafePointer<CChar>) -> UnsafeMutablePointer<FILE>
{
    return fopen(SourceCPath, Flag)
}

@_cdecl("_macos_GetFileSize")
func _macos_GetFileSize(FileHandle: UnsafeMutablePointer<FILE>) -> Int64
{
    fseeko(FileHandle, 0, SEEK_END)
    let FileSize: off_t = ftello(FileHandle)
    rewind(FileHandle)
    return FileSize
}

@_cdecl("_macos_ReadFile")
func _macos_ReadFile(
    FileHandle: UnsafeMutablePointer<FILE>, 
    Dest: UnsafeMutableRawPointer, 
    Offset: Int64, 
    BytesToRead: Int, 
    ReadFromCurrentPosition: Bool = false) -> Bool
{
    let Mode: Int32 = ReadFromCurrentPosition ? SEEK_CUR : SEEK_SET
    fseeko(FileHandle, Offset, Mode)
    let HasReadData = fread(Dest, BytesToRead, 1, FileHandle)
    return ((HasReadData != 0) ? true : false)
}

@_cdecl("_macos_RewindFileCursor")
func _macos_RewindFileCursor(FileHandle: UnsafeMutablePointer<FILE>)
{
    rewind(FileHandle)
}

@_cdecl("_macos_CloseFile")
func _macos_CloseFile(FileHandle: UnsafeMutablePointer<FILE>) -> Bool
{
    return (fclose(FileHandle) == 0)
}

@_cdecl("_macos_GetExePath")
func _macos_GetExePath(PathBuffer: UnsafeMutablePointer<CChar>, PathLength: Int) -> Int
{
    var BytesToWrite: Int = 0
    let CompletePathToExe: String? = Bundle.main.executablePath
    if let UnwrappedExecutablePath: String = CompletePathToExe
    {
        let CompletePathToExeLength: Int = UnwrappedExecutablePath.count
        let TotalCharactersToWrite: Int = (CompletePathToExeLength <= PathLength) ? CompletePathToExeLength : PathLength

        BytesToWrite = MemoryLayout<CChar>.size * (TotalCharactersToWrite + 1)
        let PointerToPath: ContiguousArray<CChar> = UnwrappedExecutablePath.utf8CString

        _ = PointerToPath.withUnsafeBufferPointer { Ptr in
            memcpy(PathBuffer, Ptr.baseAddress, BytesToWrite)
        }
    }
    
    return BytesToWrite
}

@_cdecl("_macos_CopyFile")
func _macos_CopyFile(Src: UnsafePointer<CChar>, Dst: UnsafePointer<CChar>, FailIfExists: Bool = false) -> Bool
{
    var Result: Bool = false

    let SrcPath: String = String(cString: Src)
    let DstPath: String = String(cString: Dst)

    if(SrcPath.isEmpty || DstPath.isEmpty)
    {
        print("Source or destination path is empty!")
    }
    else
    {
        if(OSFileManager.fileExists(atPath: SrcPath))
        {
            if(!(FailIfExists && OSFileManager.fileExists(atPath: DstPath)))
            {
                do
                {
                    try OSFileManager.copyItem(atPath: SrcPath, toPath: DstPath)
                    Result = true
                }
                catch
                {
                    print("Copying failed: \(error.localizedDescription)")
                }
            }
            else
            {
                print("File already exists! Cannot copy.")
            }
        }
        else
        {
            print("Source file doesn't exist. There's nothing to copy!")
        }
    }

    return Result
}

@_cdecl("_macos_CheckIfFileExists")
func _macos_CheckIfFileExists(CPath: UnsafePointer<CChar>) -> Bool
{
    let CompletePathToFile: String = String(cString: CPath)
    return OSFileManager.fileExists(atPath: CompletePathToFile)
}

@_cdecl("_macos_GetFileLastWriteTime")
func _macos_GetFileLastWriteTime(CPath: UnsafePointer<CChar>) -> Double
{
    var Timestamp: Double = 0

    autoreleasepool
    {
        let CompletePathToFile: String = String(cString: CPath)
        do
        {
            if(OSFileManager.fileExists(atPath: CompletePathToFile))
            {
                let Attributes: [FileAttributeKey : Any] = try OSFileManager.attributesOfItem(atPath: CompletePathToFile)
                let FileLastWriteTime: Date?  = Attributes[FileAttributeKey.modificationDate] as? Date
                if(FileLastWriteTime != nil)
                {
                    Timestamp = FileLastWriteTime!.timeInMillis()
                }
            }
            else
            {
                print("Cannot retrieve file last write time. File doesn't exist!")
            }
        }
        catch 
        {
            print("Failed to retrieve attributes: \(error.localizedDescription)")
        }
    }

    return Timestamp
}

@_cdecl("_macos_GetFileCreationTime")
func _macos_GetFileCreationTime(CPath: UnsafePointer<CChar>) -> Double
{
    var Timestamp: Double = 0

    autoreleasepool
    {
        let CompletePathToFile: String = String(cString: CPath)
        do
        {
            if(OSFileManager.fileExists(atPath: CompletePathToFile))
            {
                let Attributes: [FileAttributeKey : Any] = try OSFileManager.attributesOfItem(atPath: CompletePathToFile)
                let FileCreationTime: Date?  = Attributes[FileAttributeKey.creationDate] as? Date
                if(FileCreationTime != nil)
                {
                    Timestamp = FileCreationTime!.timeInMillis()
                }
            }
            else
            {
                print("Cannot retrieve file creation time. File doesn't exist!")
            }
        }
        catch 
        {
            print("Failed to retrieve attributes: \(error.localizedDescription)")
        }
    }

    return Timestamp
}
// *************************************

@main
struct Application
{
    static func main()
    {
        EntryPoint()
    }
}