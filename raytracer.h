#if !defined(COMPILER_MSVC)
#define COMPILER_MSVC 0
#endif

#if !COMPILER_MSVC
#if _MSC_VER
#undef COMPILER_MSVC
#define COMPILER_MSVC 1
#endif
#endif

#if COMPILER_MSVC
#include <intrin.h>
#endif

#include <float.h>

#define function static
#define global   static

typedef float    f32;
typedef double   f64;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t   s8;
typedef int16_t  s16;
typedef int32_t  s32;
typedef int64_t  s64;

typedef uint32_t b32;
typedef size_t   mmsz;

#define F32Max FLT_MAX
#define F32Min -FLT_MAX

#include "ray_math.h"

#define OffsetOf(StructType, MemberType) (mmsz)(&(((StructType *)0)->MemberType))

#pragma pack(push, 1)
struct bitmap_header 
{
    u16 Type;
    u32 FileSize;
    u16 Reserved1;
    u16 Reserved2;
    u32 OffsetBytes;
    
    // BITMAP INFO HEADER
    u32 StructureSize;
    s32 Width;
    s32 Height;
    u16 Planes;
    u16 BitsPerPixel;
    u32 Compression;
    u32 SizeImage;
    s32 HorizontalPixelsPerMeter;
    s32 VerticalPixelsPerMeter;
    u32 ColorIndicesUsed;
    u32 ColorIndicesImportant;
};
#pragma pack(pop)

struct pixel_color
{
    u8 b, g, r, a;
};

struct pixel_data
{
    vec4 *Data;
    u32 Count;
};

struct image_data
{
    bitmap_header Header;
    
    u32 Width;
    u32 Height;
    
    pixel_color *Data;
    u32 PixelCount;
};

struct sphere
{
    vec4 Center;
    f32 Radius;
    u32 MaterialIndex;
};

struct plane
{
    vec4 Origin;
    vec4 Normal;
    u32 MaterialIndex;
};

struct point_light
{
    vec4 Position;
    vec4 Color;
    f32 Intensity;
    f32 FallOff;
    f32 Radius;
};

struct directional_light
{
    vec4 Direction;
    vec4 Color;
};

struct material
{
    vec4 Albedo;
    vec4 Emissive;
    f32 EmissiveStrength;
    f32 Metalness;
    f32 Roughness;
};

struct scene_storage
{
    material Materials[16];
    u32 MaterialsCount;
    
    sphere Spheres[16];
    u32 SpheresCount;
    
    plane Planes[16];
    u32 PlanesCount;
    
    point_light Pointlights[4];
    u32 PointlightsCount; 
    
    directional_light Directionallights[2];
    u32 DirectionallightsCount;
};

enum entity_type
{
    EntityType_Unknown,
    
    EntityType_Sphere,
    EntityType_Plane,
};

struct hit_info
{
    vec4 HitPosition;
    vec4 HitSurfaceNormal;
    u32 MaterialIndex;
    b32 MadeContact;
    
    u32 EntityIndex;
    entity_type EntityType;
};

struct pixel_info
{
    f32 PixelWidthSize;
    f32 PixelHeightSize;
    f32 HalfPixelWSize;
    f32 HalfPixelHSize;
};

struct viewport_info
{
    f32 ViewportWidth;
    f32 ViewportHeight;
    f32 HalfViewportWidth;
    f32 HalfViewportHeight;
};

struct raytracer_statistics
{
    f32 AverageTimePerPixel_MS;
    f32 AverageTimePerInitialRay_MS;
    f32 AverageTimePerRayBounce_MS;

    f32 TotalRenderTime_S;
    u32 TotalPassedIntersectionTests;
    u32 TotalValidIntersectionTests;
    u32 TotalIntersectionTests;
    u32 TotalPixelsTraced;
    u32 AverageNumberOfBouncesPerPixel;
};

struct job_pixel_data
{
    f32 Gamma;
    u32 NumberOfRays;
    u32 TotalBouncesPerPixel;
    u32 TotalFrames;
    
    pixel_data OutputData;
    u32 DataIndex;
    
    pixel_info PixelInfo;
    viewport_info ViewportInfo;
    
    f32 ViewportX;
    f32 ViewportY;
    f32 FarPlane;
    vec4 ViewportCenter;
    vec4 CameraX;
    vec4 CameraY;
    vec4 CameraPos;

    raytracer_statistics *RaytracerStatistics;
    
    scene_storage *WorldStorage;
};

/*
    Retrieves the frequency of the performance counter. The frequency of the performance counter is fixed 
    at system boot and is consistent across all processors. Therefore, the frequency need only be queried 
    upon application initialization, and the result can be cached.
*/
function f64
GetPerformanceCounterFrequency()
{
    LARGE_INTEGER _PerformanceFrequency;
    assert(QueryPerformanceFrequency(&_PerformanceFrequency));
    
    f64 PerfCountFrequency = (f64)_PerformanceFrequency.QuadPart;
    return(PerfCountFrequency);
}

f64 PerformanceFrequency = GetPerformanceCounterFrequency();

function f64
GetTimestamp()
{
    LARGE_INTEGER PerformanceCount;
    assert(QueryPerformanceCounter(&PerformanceCount));
    
    f64 Result = (f64)PerformanceCount.QuadPart;
    return(Result);
}

struct timer
{
    b32 IsRunning;
    f64 Timestamp;
};

function void
StartTimer(timer *Timer)
{
    Timer->IsRunning = true;
    Timer->Timestamp = GetTimestamp();
}

function void
StopTimer(timer *Timer)
{
    Timer->IsRunning = false;
    Timer->Timestamp = GetTimestamp() - Timer->Timestamp;
}

function void
ResetTimer(timer *Timer)
{
    Timer->Timestamp = GetTimestamp();
}

function f32
GetCurrentTimeInSeconds(timer *Timer)
{
    f32 Result = (f32)((GetTimestamp() - Timer->Timestamp) / PerformanceFrequency);
    return(Result);
}

function f32
GetCurrentTimeInMilliseconds(timer *Timer)
{
    f32 Result = (f32)((GetTimestamp() - Timer->Timestamp) / PerformanceFrequency);
    return(Result * 1000);
}

function f32
GetTimeInSeconds(timer *Timer)
{
    f32 Result = Result = (f32)(Timer->Timestamp / PerformanceFrequency);
    return(Result);
}

function f32
GetTimeInMilliseconds(timer *Timer)
{
    f32 Result = Result = GetTimeInSeconds(Timer) * 1000;
    return(Result);
}

struct ticket_mutex
{
    u64 volatile CurrentTicket;
    u64 volatile ServedTicket;
};

function u64
AtomicAddU64(u64 volatile *Value, u64 Addend)
{
    u64 Result = _InterlockedExchangeAdd64((__int64 volatile *)Value, Addend);
    return(Result);
}

function void
TicketMutexLock(ticket_mutex *Mutex)
{
    u64 TicketNumber = AtomicAddU64(&Mutex->CurrentTicket, 1);
    while (TicketNumber != Mutex->ServedTicket); // TODO(Cristian): _mm_pause here?
}

function void
TicketMutexUnlock(ticket_mutex *Mutex)
{
    AtomicAddU64(&Mutex->ServedTicket, 1);
}