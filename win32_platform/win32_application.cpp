#include "win32_application.h"

platform_api Platform;
global_variable memory_arena DefaultPlatformArena = {};
global_variable win32_state GlobalWin32State;

// NOTE(Cristian): We prefer discrete GPUs (in laptops) where available
extern "C"
{
    __declspec(dllexport) DWORD NvOptimusEnablement = 0x01;
    __declspec(dllexport) DWORD AmdPowerXpressRequestHighPerformance = 0x01;
}

function
PLATFORM_ATOMIC_EXCHANGE_U32(Win32AtomicExchangeU32)
{
    u32 Result = InterlockedExchange((LONG volatile *)Value, New);
    return(Result);
}

function
PLATFORM_ATOMIC_ADD_U32(Win32AtomicAddU32)
{
    u32 Result = InterlockedExchangeAdd((LONG volatile *)Value, Addend);
    return(Result);
}

function
PLATFORM_ATOMIC_COMPARE_EXCHANGE_U32(Win32AtomicCompareExchangeU32)
{
    u32 Result = InterlockedCompareExchange((LONG volatile *)Destination, Exchange, Comparand);
    return(Result);
}

function
PLATFORM_ATOMIC_INCREMENT_U32(Win32AtomicIncrementU32)
{
    u32 Result = InterlockedIncrement((LONG volatile *)Addend);
    return(Result);
}

function
PLATFORM_ATOMIC_DECREMENT_U32(Win32AtomicDecrementU32)
{
    u32 Result = InterlockedDecrement((LONG volatile *)Addend);
    return(Result);
}

function 
PLATFORM_ATOMIC_EXCHANGE_U64(Win32AtomicExchangeU64)
{
    u64 Result = InterlockedExchange64((LONG64 volatile *)Value, New);
    return(Result);
}

function 
PLATFORM_ATOMIC_ADD_U64(Win32AtomicAddU64)
{
    u64 Result = InterlockedExchangeAdd64((LONG64 volatile *)Value, Addend);
    return(Result);
}

function
PLATFORM_ATOMIC_COMPARE_EXCHANGE_U64(Win32AtomicCompareExchangeU64)
{
    u64 Result = InterlockedCompareExchange64((LONG64 volatile *)Destination, Exchange, Comparand);
    return(Result);
}

function
PLATFORM_ATOMIC_INCREMENT_U64(Win32AtomicIncrementU64)
{
    u64 Result = InterlockedIncrement64((LONG64 volatile *)Addend);
    return(Result);
}

function
PLATFORM_ATOMIC_DECREMENT_U64(Win32AtomicDecrementU64)
{
    u64 Result = InterlockedDecrement64((LONG64 volatile *)Addend);
    return(Result);
}

function
PLATFORM_ALLOCATE_MEMORY(Win32AllocateMemory)
{
    memory_size TotalSize = Size + sizeof(win32_memory_block);
    memory_size BlockOffset = sizeof(win32_memory_block);
    memory_size ProtectOffset = 0;
    
    // TODO(Cristian): Should retrieve the page size from the OS instead of hardcoding it
    memory_size PageSize = 4096;
    if(AllocationFlags & PlatformFlags_UnderflowCheck)
    {
        TotalSize = 2 * PageSize + Size;
        BlockOffset = 2 * PageSize;
        ProtectOffset = PageSize;
    }
    else if(AllocationFlags & PlatformFlags_OverflowCheck)
    {
        memory_size PageAlignedSize = AlignToPow2(Size, PageSize);
        TotalSize = PageSize + PageAlignedSize + PageSize;
        BlockOffset = PageSize + PageAlignedSize - Size;
        ProtectOffset = PageSize + PageAlignedSize;
    }
    
    win32_memory_block *NewBlock = (win32_memory_block *)VirtualAlloc(0, TotalSize, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
    Assert(NewBlock);
    
    NewBlock->Block.Base = (ump)NewBlock + BlockOffset;
    
    Assert(NewBlock->Block.Used == 0);
    Assert(NewBlock->Block.PrevBlock == 0);
    
    if(AllocationFlags & (PlatformFlags_UnderflowCheck | PlatformFlags_OverflowCheck))
    {
        DWORD OldProtect = 0;
        BOOL ProtectSuccess = VirtualProtect((void *)((ump)NewBlock + ProtectOffset), PageSize, PAGE_NOACCESS, &OldProtect);
        Assert(ProtectSuccess);
    }
    
    win32_memory_block *Sentinel = &GlobalWin32State.MemorySentinel;
    NewBlock->Next = Sentinel;
    NewBlock->Block.Size = Size;
    NewBlock->Block.Flags = AllocationFlags;
    
    TicketMutexLock(&GlobalWin32State.MemoryMutex);
    NewBlock->Prev = Sentinel->Prev;
    NewBlock->Prev->Next = NewBlock;
    NewBlock->Next->Prev = NewBlock;
    TicketMutexUnlock(&GlobalWin32State.MemoryMutex);
    
    platform_memory_block *PlatformBlock = &NewBlock->Block;
    return(PlatformBlock);
}

function void
Win32FreeMemoryBlock(win32_memory_block *Block)
{
    TicketMutexLock(&GlobalWin32State.MemoryMutex);
    Block->Prev->Next = Block->Next;
    Block->Next->Prev = Block->Prev;
    TicketMutexUnlock(&GlobalWin32State.MemoryMutex);
    
    b32 Result = VirtualFree(Block, 0, MEM_RELEASE);
    Assert(Result);
}

function
PLATFORM_DEALLOCATE_MEMORY(Win32DeallocateMemory)
{
    win32_memory_block *Block = (win32_memory_block *)PlatformBlock;
    if(Block)
    {
        Win32FreeMemoryBlock(Block);
    }
}

function
PLATFORM_ZERO_MEMORY_BLOCK(Win32ZeroMemoryBlock)
{
    return(SecureZeroMemory(Block, Size));
}

// NOTE(Cristian): Apparently CopyMemory & RtlCopyMemory are defined as memcpy in the windows headers, so... here we go?
function
PLATFORM_COPY_MEMORY(Win32CopyMemory)
{
    memcpy_s(Dest, DestSize, Src, SrcSize);
}

function
PLATFORM_CHECK_IF_FILE_EXISTS(Win32CheckIfFileExists)
{
    b32 Result = false;
    
    DWORD Attributes = GetFileAttributes(Path);
    if(Attributes != INVALID_FILE_ATTRIBUTES && !(Attributes & FILE_ATTRIBUTE_DIRECTORY))
    {
        Result = true;
    }
    return(Result);
}

function
PLATFORM_GET_FILE_LAST_WRITE_TIME(Win32GetFileLastWriteTime)
{
    u64 LastWriteTime = {};
    
    WIN32_FILE_ATTRIBUTE_DATA Data;
    if(GetFileAttributesExA(Filename, GetFileExInfoStandard, &Data))
    {
        LastWriteTime = (((u64)Data.ftLastWriteTime.dwHighDateTime) << (u64)32) | (u64)Data.ftLastWriteTime.dwLowDateTime;
    }
    
    return(LastWriteTime);
}

function b32
Win32FileTimeIsValid(u64 Time)
{
    return(Time != 0);
}

function timestamps_comparison
Win32CompareTimestamps(u64 First, u64 Second)
{
    timestamps_comparison Result = Timestamps_Equal;
    Result = (First < Second) ? Timestamps_FirstIsEarlier : Timestamps_FirstIsLater;

    return(Result);
}

function void
Win32UnloadCode(win32_dynamic_code *DynamicCode)
{
    if(DynamicCode->DLL)
    {
        // TODO(Cristian): This might cause problems in the future with pointers to strings inside the DLL.
        FreeLibrary(DynamicCode->DLL);
    }
    
    DynamicCode->DLL = 0;
    DynamicCode->IsValid = false;
    
    for(u32 FunctionIndex = 0;
        FunctionIndex < DynamicCode->FunctionsCount;
        ++FunctionIndex)
    {
        DynamicCode->Functions[FunctionIndex] = 0;
    }
}

function void
Win32ConcatEXEPath(win32_state *State, const char *DLLName, char *Dest, memory_size DestSize)
{
    char PathToEXE[MAX_PATH];
    memcpy(PathToEXE, State->EXEPath, State->OnePastLastEXEPathSlash - State->EXEPath);
    *(PathToEXE + (State->OnePastLastEXEPathSlash - State->EXEPath)) = 0;
    
    snprintf(Dest, DestSize, "%s%s", PathToEXE, DLLName);
}

function void
Win32ConcatEXEPath(win32_state *State, const char *DLLName, u32 UniqueTag, char *Dest, memory_size DestSize)
{
    char PathToEXE[MAX_PATH];
    memcpy(PathToEXE, State->EXEPath, State->OnePastLastEXEPathSlash - State->EXEPath);
    *(PathToEXE + (State->OnePastLastEXEPathSlash - State->EXEPath)) = 0;
    
    snprintf(Dest, DestSize, "%s%d_%s", PathToEXE, UniqueTag, DLLName);
}

function
PLATFORM_CREATE_FILE(Win32CreateFile)
{
    FAIL HERE
}

function
PLATFORM_COPY_FILE(Win32CopyFile)
{
    b32 Result = CopyFile(Src, Dest, FailIfExists);
    return(Result);
}

function void
Win32LoadCode(win32_state *State, win32_dynamic_code *DynamicCode)
{
    char *SourceDLLPath = DynamicCode->SourceDLLPath;

    if(!Win32CheckIfFileExists(DynamicCode->LockFilePath) &&
        Win32CheckIfFileExists(SourceDLLPath))
    {
        char TempDLLPath[MAX_PATH];
        DynamicCode->LastDLLWriteTime = Win32GetFileLastWriteTime(SourceDLLPath);

        b32 AttemptsFailed = true;
        for(u32 AttemptIndex = 0;
            AttemptIndex < 128;
            ++AttemptIndex)
        {
            Win32ConcatEXEPath(State, DynamicCode->TransientDLLName, DynamicCode->TempDLLNumber, TempDLLPath, sizeof(TempDLLPath));
            
            if(++DynamicCode->TempDLLNumber >= 1024)
            {
                DynamicCode->TempDLLNumber = 0;
            }
            
            if(Win32CopyFile(SourceDLLPath, TempDLLPath, false))
            {
                AttemptsFailed = false;
                break;
            }
        }

        if(!AttemptsFailed)
        {
            DynamicCode->DLL = LoadLibrary(TempDLLPath);
            if (DynamicCode->DLL)
            {
                DynamicCode->IsValid = true;
                
                for(u32 FunctionIndex = 0;
                    FunctionIndex < DynamicCode->FunctionsCount;
                    ++FunctionIndex)
                {
                    void *Function = GetProcAddress(DynamicCode->DLL, DynamicCode->FunctionNames[FunctionIndex]);
                    if(Function)
                    {
                        DynamicCode->Functions[FunctionIndex] = Function;
                    }
                    else
                    {
                        // TODO(Cristian): Could maybe find a way to only use valid functions.. ?
                        // Maybe only fail if core functionality is not loaded?
                        DynamicCode->IsValid = false;
                        break;
                    }
                }
            }
        }

        if(!DynamicCode->IsValid)
        {
            Win32UnloadCode(DynamicCode);
        }
    }
}

function void
Win32ReloadCode(win32_state *State, win32_dynamic_code *DynamicCode)
{
    if(!Win32CheckIfFileExists(DynamicCode->LockFilePath))
    {
        u64 CurrentDLLWriteTime = Win32GetFileLastWriteTime(DynamicCode->SourceDLLPath);
        if(Win32FileTimeIsValid(DynamicCode->LastDLLWriteTime) && Win32FileTimeIsValid(CurrentDLLWriteTime))
        {
            timestamps_comparison Comparison = Win32CompareTimestamps(DynamicCode->LastDLLWriteTime, CurrentDLLWriteTime);
            b32 DLLNeedsToReload = (Comparison == Timestamps_FirstIsEarlier);
            if(DLLNeedsToReload)
            {
                Win32UnloadCode(DynamicCode);
                Win32LoadCode(State, DynamicCode);
            }
        }
    }
}

function void
Win32GetEXEPath(win32_state *State)
{
    u32 MainEXEFullPathSize = GetModuleFileNameA(0, State->EXEPath, sizeof(State->EXEPath));
    State->OnePastLastEXEPathSlash = State->EXEPath;
    for(char *At = State->EXEPath;
        *At;
        ++At)
    {
        if(*At == '\\')
        {
            State->OnePastLastEXEPathSlash = At + 1;
        }
    }
}

function win32_processor_information
Win32GetProcessorInformation(win32_state *State, memory_arena *Arena = &DefaultPlatformArena)
{
    win32_processor_information Result = {};

    DWORD ReturnLength = 0;
    PSYSTEM_LOGICAL_PROCESSOR_INFORMATION Buffer = 0;
    GetLogicalProcessorInformation(Buffer, &ReturnLength);

    BeginTemporaryMemory(Arena);
    Buffer = (PSYSTEM_LOGICAL_PROCESSOR_INFORMATION)PushSlice(Arena, ReturnLength);

    b32 FnStatus = GetLogicalProcessorInformation(Buffer, &ReturnLength);
    if(FnStatus && (ReturnLength >= sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION)))
    {
        u32 Offset = 0;
        u32 CurrentOffset = 0;
        u32 CacheIndex = 0;
        PSYSTEM_LOGICAL_PROCESSOR_INFORMATION BufferIterator = Buffer;
        while(CurrentOffset <= ReturnLength)
        {
            switch(BufferIterator->Relationship)
            {
                case RelationNumaNode:
                {
                    Result.NumaNodeCount++;
                } break;

                case RelationProcessorCore:
                {
                    // A hyperthreaded core supplies more than one logical processor.
                    u32 LogicalProcessorCount = CountSetBits(BufferIterator->ProcessorMask);
                    if(LogicalProcessorCount > 0)
                    {
                        Result.ProcessorCoreCount++;
                        Result.LogicalProcessorCount += LogicalProcessorCount;
                    }
                } break;

                // TODO(Cristian): Cache lines should be read per core and associated as such
                // WARNING(Cristian): Cache line descriptions might not be read properly. Need to check this out sometime!
                case RelationCache:
                {
                    CACHE_DESCRIPTOR *Cache = &BufferIterator->Cache;
                    if(Cache)
                    {
                        win32_processor_cache_descriptor *CacheDescriptor = &Result.ProcessorCacheDescriptors[CacheIndex++];
                        if(CacheDescriptor)
                        {
                            CacheDescriptor->Size = (u32)Cache->Size;
                            CacheDescriptor->Level = (u8)Cache->Level;
                            CacheDescriptor->LineSize = (u16)Cache->LineSize;
                            CacheDescriptor->Associativity = (u8)Cache->Associativity;
                            CacheDescriptor->ProcessorCacheType = (win32_processor_cache_type)Cache->Type;
                        }

                        if(Cache->Level == 1)
                        {
                            Result.ProcessorL1CacheCount++;
                        }
                        else if(Cache->Level == 2)
                        {
                            Result.ProcessorL2CacheCount++;
                        }
                        else if(Cache->Level == 3)
                        {
                            Result.ProcessorL3CacheCount++;
                        }
                    }
                    else
                    {
                        // TODO: LOG ERROR HERE
                    }
                } break;

                case RelationProcessorPackage:
                {
                    Result.ProcessorPackageCount++;
                } break;

                default:
                {
                    // TODO: LOG ERROR HERE
                } break;
            }

            CurrentOffset += (Offset + sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION));
            ++BufferIterator;
        }
    }
    else
    {
        // TODO: LOG ERROR HERE    
    }

    EndTemporaryMemory(Arena);

    return(Result);
}

function
PLATFORM_CREATE_SEMAPHORE(Win32CreateSemaphore)
{
    // TODO(Cristian): Maybe should modify this in the future to have more control over semaphore creation from app side

    /*  
        [SemaphoreAttributes]
        A pointer to a SECURITY_ATTRIBUTES structure. If this parameter is NULL, 
        the semaphore handle cannot be inherited by child processes.

        [Flags]
        This parameter is reserved and must be 0.

        [DesiredAccess]
        The access mask for the semaphore object.
        SEMAPHORE_MODIFY_STATE => Modify state access, which is required for the ReleaseSemaphore function.
    */

    u32 Flags = 0;
    u32 DesiredAccess = SEMAPHORE_MODIFY_STATE;
    SECURITY_ATTRIBUTES *SemaphoreAttributes = nullptr;

    void *Result = (void *)CreateSemaphoreExA(SemaphoreAttributes, InitialCount, MaximumCount, Name, Flags, DesiredAccess);
    Assert(Result != nullptr);
    return(Result);
}


function
PLATFORM_SEMAPHORE_WAIT(Win32WaitForSingleObject)
{
    // NOTE(Cristian): There's no need to check for nullptr and fail the function here. OS API does this for us
    if(!Handle)
    {
        // LOG ERROR HERE
    }

    u32 Result = WaitForSingleObjectEx(Handle, dwMilliseconds, bAlertable);
    return(Result);
}

function
PLATFORM_RELEASE_SEMAPHORE(Win32ReleaseSemaphore)
{
    if(!Handle)
    {
        // LOG ERROR HERE
    }

    b32 Result = ReleaseSemaphore(Handle, ReleaseCount, (LONG *)PreviousCount);
    return(Result);
}

function
PLATFORM_GET_LOGICAL_PROCESSOR_COUNT(Win32GetLogicalProcessorCount)
{
    u32 Result = GlobalWin32State.ProcessorInfo.LogicalProcessorCount;
    Assert(Result != 0);
    return(Result);
}

function
PLATFORM_CREATE_THREAD(Win32CreateThread)
{
    /*
        [ThreadAttributes]
        A pointer to a SECURITY_ATTRIBUTES structure that determines whether the returned handle 
        can be inherited by child processes. If lpThreadAttributes is NULL, the handle cannot be inherited.

        [StackSize]
        The initial size of the stack, in bytes. The system rounds this value to the nearest page. 
        If this parameter is zero, the new thread uses the default size for the executable
    */

    mmsz ThreadStackSize = 0;
    SECURITY_ATTRIBUTES *ThreadAttributes = nullptr;

    u32 CreationArg = ThreadCreationState_RunImmediately;
    switch(CreationArgs.CreationState)
    {
        case ThreadCreationState_CreateSuspended:
        {
            CreationArg = CREATE_SUSPENDED;
        } break;

        case ThreadCreationState_StackSizeReservation:
        {
            CreationArg = STACK_SIZE_PARAM_IS_A_RESERVATION;
        } break;
    }

    if(CreationArg == ThreadCreationState_StackSizeReservation)
    {
        ThreadStackSize = CreationArgs.StackSize;
    }

    // TODO(Cristian): Should maybe have separate OS and platform thread IDs instead of returning OS ID
    thread_info Result = {};
    Result.Handle = (void *)CreateThread(ThreadAttributes,
                                        ThreadStackSize,
                                        (LPTHREAD_START_ROUTINE)CreationArgs.StartAddress,
                                        CreationArgs.ThreadParams,
                                        CreationArg,
                                        (LPDWORD)&Result.ThreadID);
    Assert(Result.Handle != nullptr);

    return(Result);
}

function
PLATFORM_DO_NEXT_AVAILABLE_JOB_ENTRY(Win32DoNextAvailableJobEntry)
{
    b32 DidWork = false;
    
    u32 OriginalNextEntryToRead = Queue->NextJobEntryToRead;
    if (OriginalNextEntryToRead != Queue->NextJobEntryToWrite)
    {   
        u32 EntryIndex = InterlockedCompareExchange((LONG volatile *)&Queue->NextJobEntryToRead, 
                                                    (OriginalNextEntryToRead + 1) % Queue->JobEntriesCapacity, 
                                                    OriginalNextEntryToRead);

        if (EntryIndex == OriginalNextEntryToRead)
        {
            job_queue_entry JobEntry = Queue->JobEntries[EntryIndex];
            JobEntry.Callback(JobEntry.Payload, JobEntry.CallerThreadIndex, CallerThreadIndex);

            InterlockedDecrement((LONG volatile *)&Queue->AvailableEntriesCount);
            
            DidWork = true;
        }
    }
    
    return(DidWork);
}

function DWORD WINAPI
Win32JobQueueThreadProc(void *lpParameter)
{
    thread_params *ThreadInfo = (thread_params *)lpParameter;
    if(ThreadInfo)
    {
        job_queue *Queue = ThreadInfo->Queue;
        if(Queue)
        {
            for (;;)
            {
                b32 DidWork = Win32DoNextAvailableJobEntry(Queue, ThreadInfo->LogicalThreadIndex);
                if (!DidWork)
                {
                    WaitForSingleObjectEx(Queue->SemaphoreHandle, INFINITE_TIMEOUT, false);
                }
            }   
        }
    }
    
    return(0);
}

function
PLATFORM_CLOSE_HANDLE(Win32CloseHandle)
{
    if(!Handle)
    {
        // LOG ERROR HERE
    }

    b32 Result = CloseHandle((HANDLE)Handle);
    return(Result);
}

/*
    TODO(Cristian): Should maybe look into having a high and a low priority queue...?
    Would that even be useful for an offline raytracer?
*/ 
function
PLATFORM_CREATE_JOB_QUEUE(Win32CreateJobQueue)
{
    u32 ThreadCount = Win32GetLogicalProcessorCount() - 1; // NOTE(Cristian): Leave one logical core free to the OS
    Assert(ThreadCount != 0);

    Queue->ThreadCount = ThreadCount;

    u32 InitialCount = 0;
    Queue->SemaphoreHandle = Win32CreateSemaphore(InitialCount, ThreadCount, 0);
    Assert(Queue->SemaphoreHandle);

    Queue->JobEntriesCapacity = 512;
    Queue->JobEntries = PushArray(&DefaultPlatformArena, Queue->JobEntriesCapacity, job_queue_entry);
    Assert(Queue->JobEntries);
    
    thread_params *ThreadParams = PushArray(&DefaultPlatformArena, ThreadCount, thread_params);
    Assert(ThreadParams);

    for (u32 ThreadIndex = 0; ThreadIndex < ThreadCount; ++ThreadIndex)
    {
        thread_params *CurrentThreadParams = ThreadParams + ThreadIndex;
        CurrentThreadParams->LogicalThreadIndex = ThreadIndex + 1;
        CurrentThreadParams->Queue = Queue;

        DWORD ThreadID = 0;
        HANDLE ThreadHandle = CreateThread(
            0,
            0,
            Win32JobQueueThreadProc,
            CurrentThreadParams,
            0,
            &ThreadID
        );

        if(ThreadHandle)
        {
            HRESULT r = SetThreadDescription(ThreadHandle, L"QueueWorkerThread");

            CloseHandle(ThreadHandle);
        }
    }
}

function
PLATFORM_ADD_JOB_ENTRY(Win32AddJobEntry)
{
    u32 NewNextJobEntryToWrite = (Queue->NextJobEntryToWrite + 1) % Queue->JobEntriesCapacity;
    if(NewNextJobEntryToWrite != Queue->NextJobEntryToRead)
    {
        job_queue_entry *JobEntry = Queue->JobEntries + Queue->NextJobEntryToWrite;
        JobEntry->Callback = Callback;
        JobEntry->Payload = Payload;
        JobEntry->CallerThreadIndex = CallerThreadIndex;

        InterlockedIncrement((LONG volatile *)&Queue->AvailableEntriesCount);
        
        Queue->NextJobEntryToWrite = NewNextJobEntryToWrite;

        u32 ReleaseCount = 1;
        ReleaseSemaphore(Queue->SemaphoreHandle, ReleaseCount, nullptr);
    }
    else
    {
        // TODO: LOG ERROR HERE
    }
}

function
PLATFORM_COMPLETE_ALL_JOB_QUEUE_WORK(Win32CompleteAllJobQueueWork)
{
    while(Queue->AvailableEntriesCount)
    {
        Win32DoNextAvailableJobEntry(Queue, CallerThreadIndex);
    }
}

/*
    Retrieves the frequency of the performance counter.
    It's fixed at system boot and consistent across all processors.
    Should only be queried on application init and be cached.
*/
function void
Win32GetPerformanceCounterFrequency()
{
    LARGE_INTEGER _PerformanceFrequency = {};
    BOOL SupportsHighResolutionPerformanceCounter = QueryPerformanceFrequency(&_PerformanceFrequency);
    Assert(SupportsHighResolutionPerformanceCounter);
    
    GlobalWin32State.PerformanceCounterFrequency = (f64)_PerformanceFrequency.QuadPart;
}

function
PLATFORM_GET_TIMESTAMP(Win32GetTimestamp)
{
    LARGE_INTEGER PerformanceCount = {};
    BOOL RetrievedCounter = QueryPerformanceCounter(&PerformanceCount);
    Assert(RetrievedCounter);
    
    f64 Result = (f64)PerformanceCount.QuadPart;
    return(Result);
}

function
PLATFORM_GET_TIME_IN_SECONDS(Win32GetTimeInSeconds)
{
    f64 Result = Timestamp / GlobalWin32State.PerformanceCounterFrequency;
    return(Result);
}

function void
Win32InitializePlatformAPI()
{
    platform_api PlatformAPI = {};
    PlatformAPI.CopyFile                    = Win32CopyFile;
    PlatformAPI.CheckIfFileExists           = Win32CheckIfFileExists;
    PlatformAPI.GetFileLastWriteTime        = Win32GetFileLastWriteTime;
    PlatformAPI.AllocateMemory              = Win32AllocateMemory;
    PlatformAPI.DeallocateMemory            = Win32DeallocateMemory;
    PlatformAPI.ZeroMemoryBlock             = Win32ZeroMemoryBlock;
    PlatformAPI.CopyMemory                  = Win32CopyMemory;

    PlatformAPI.AtomicExchangeU32           = Win32AtomicExchangeU32;
    PlatformAPI.AtomicAddU32                = Win32AtomicAddU32;
    PlatformAPI.AtomicCompareExchangeU32    = Win32AtomicCompareExchangeU32;
    PlatformAPI.AtomicIncrementU32          = Win32AtomicIncrementU32;
    PlatformAPI.AtomicDecrementU32          = Win32AtomicDecrementU32;

    PlatformAPI.AtomicExchangeU64           = Win32AtomicExchangeU64;
    PlatformAPI.AtomicAddU64                = Win32AtomicAddU64;
    PlatformAPI.AtomicCompareExchangeU64    = Win32AtomicCompareExchangeU64;
    PlatformAPI.AtomicIncrementU64          = Win32AtomicIncrementU64;
    PlatformAPI.AtomicDecrementU64          = Win32AtomicDecrementU64;

    PlatformAPI.CreateSemaphore             = Win32CreateSemaphore;
    PlatformAPI.SemaphoreWait               = Win32WaitForSingleObject;
    PlatformAPI.ReleaseSemaphore            = Win32ReleaseSemaphore;

    PlatformAPI.GetLogicalProcessorCount    = Win32GetLogicalProcessorCount;
    PlatformAPI.CreateThread                = Win32CreateThread;
    PlatformAPI.CloseHandle                 = Win32CloseHandle;

    PlatformAPI.CreateJobQueue              = Win32CreateJobQueue;
    PlatformAPI.DoNextAvailableJobEntry     = Win32DoNextAvailableJobEntry;
    PlatformAPI.AddJobEntry                 = Win32AddJobEntry;
    PlatformAPI.CompleteAllJobQueueWork     = Win32CompleteAllJobQueueWork;

    PlatformAPI.GetTimestamp                = Win32GetTimestamp;
    PlatformAPI.GetTimeInSeconds            = Win32GetTimeInSeconds;

    Platform = PlatformAPI;
}

s32 CALLBACK
WinMain(HINSTANCE InstanceHandle,
        HINSTANCE PrevInstanceHandle,
        LPSTR     CommandLine,
        s32       ShowCode)
{
    Win32InitializePlatformAPI();

    win32_state *State = &GlobalWin32State;
    State->MemorySentinel.Prev = &State->MemorySentinel;
    State->MemorySentinel.Next = &State->MemorySentinel;
    InitializeArena(&DefaultPlatformArena);

    State->ProcessorInfo = Win32GetProcessorInformation(State);
    Win32GetPerformanceCounterFrequency();
    Win32GetEXEPath(State);

    char ApplicationDLLFullPath[MAX_PATH];
    Win32ConcatEXEPath(State, "application.dll", ApplicationDLLFullPath, sizeof(ApplicationDLLFullPath));

    char LockFileFullPath[MAX_PATH];
    Win32ConcatEXEPath(State, "lock.tmp", LockFileFullPath, sizeof(LockFileFullPath));

    win32_application_function_table ApplicationFunctions = {};
    win32_dynamic_code ApplicationCode = {};
    ApplicationCode.SourceDLLPath = ApplicationDLLFullPath;
    ApplicationCode.TransientDLLName = "application_temp.dll";
    ApplicationCode.LockFilePath = LockFileFullPath;
    ApplicationCode.FunctionsCount = ArrayCapacity(Win32ApplicationFunctionTableNames);
    ApplicationCode.FunctionNames = Win32ApplicationFunctionTableNames;
    ApplicationCode.Functions = (void **)&ApplicationFunctions;
    Win32LoadCode(State, &ApplicationCode);

    platform_memory ApplicationMemory = {};
    ApplicationMemory.ApplicationRunning = true;
    ApplicationMemory.PlatformAPI = Platform;
    ApplicationMemory.PlatformArena = &DefaultPlatformArena;

    if(ApplicationFunctions.Init)
    {
        ApplicationFunctions.Init(&ApplicationMemory);
    }

    while(ApplicationMemory.ApplicationRunning)
    {
        Win32ReloadCode(State, &ApplicationCode);

        if(ApplicationFunctions.UpdateAndRender)
        {
            ApplicationFunctions.UpdateAndRender(&ApplicationMemory, 0);
        }
    }

    if(ApplicationFunctions.Shutdown)
    {
        ApplicationFunctions.Shutdown();
    }

    ExitProcess(0);
}