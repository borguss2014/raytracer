#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>

#include "../application/platform_shared.h"
#include "../application/application_memory.h"

typedef b32 (WINAPI PFN_GLPI)(SYSTEM_LOGICAL_PROCESSOR_INFORMATION *, DWORD *);

enum win32_processor_cache_type
{
    CacheType_None = 0,
    
    CacheType_Unified,
    CacheType_Instruction,
    CacheType_Data,
    CacheType_Trace,
    CacheType_Unknown
};

struct win32_processor_cache_descriptor
{
    u8 Level;
    u8 Associativity;
    u16 LineSize;
    u32 Size;
    win32_processor_cache_type ProcessorCacheType;
};

struct win32_processor_information
{
    u32 NumaNodeCount;
    u32 ProcessorCoreCount;
    u32 LogicalProcessorCount;
    u32 ProcessorL1CacheCount;
    u32 ProcessorL2CacheCount;
    u32 ProcessorL3CacheCount;
    u32 ProcessorPackageCount;

    win32_processor_cache_descriptor ProcessorCacheDescriptors[256];
};

struct win32_memory_block
{
    platform_memory_block Block;
    win32_memory_block *Prev;
    win32_memory_block *Next;
};

struct win32_state
{    
    char EXEPath[MAX_PATH];
    char *OnePastLastEXEPathSlash;
    
    ticket_mutex MemoryMutex;
    win32_memory_block MemorySentinel;

    win32_processor_information ProcessorInfo;

    f64 PerformanceCounterFrequency;
};

struct win32_dynamic_code
{
    HMODULE DLL;
    
    char *SourceDLLPath;
    char *LockFilePath;
    const char *TransientDLLName;
    
    u32 TempDLLNumber;
    
    u64 LastDLLWriteTime;
    
    u32 FunctionsCount;
    const char **FunctionNames;
    void **Functions;
    
    b32 IsValid;
};

global_variable const char *Win32ApplicationFunctionTableNames[] =
{
    "Init",
    "UpdateAndRender",
    "Shutdown"
};

struct win32_application_function_table
{
    app_init *Init;
    app_update_and_render *UpdateAndRender;
    app_shutdown *Shutdown;
};