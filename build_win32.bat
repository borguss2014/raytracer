@echo off

REM ==================================================== MSVC ====================================================
set OptimizationMode=Optimization_Debug
REM set OptimizationMode=%Optimization_Optimized% -O2
set DisabledWarnings=-WL -WX -W4 -wd4100 -wd4505 -wd4101 -wd4189 -wd4702 -wd4127 -wd4311 -wd4302 -wd4201 -wd4324 -wd4723 -wd4701 -wd4703
set CommonCompilerFlags=-diagnostics:column -nologo -GF- -Gy- /Fo.\tmp\ -FC -Z7 -Od -Oi /arch:AVX2
set CommonCompilerFlags=%CommonCompilerFlags% %DisabledWarnings% /D%OptimizationMode%
set CommonLinkerFlags=-incremental:no -opt:ref /CGTHREADS:8 user32.lib gdi32.lib

if not exist .\build\ (mkdir .\build\)
pushd .\build\

if not exist .\tmp\ (mkdir .\tmp\)

del *.pdb > NUL 2> NUL

REM x64 Build
echo [ PROJECT BUILD ]

echo WAITING FOR PDB > lock.tmp
set LinkerExports=-EXPORT:Init -EXPORT:UpdateAndRender -EXPORT:Shutdown
call cl %CommonCompilerFlags% -MTd -I ..\win32_platform\libs\ ..\application\application.cpp -LD /link %LinkerExports% /implib:.\tmp\application.lib -PDB:application_%random%.pdb %CommonLinkerFlags%
set DLL_ERRORS=%errorlevel%
del lock.tmp

call cl %CommonCompilerFlags% -MTd -I ..\win32_platform\libs\ ..\win32_platform\win32_application.cpp /link /implib:.\tmp\win32_application.lib %CommonLinkerFlags%
set MAIN_ERRORS=%errorlevel%
echo.

popd

if %DLL_ERRORS% EQU 0 (
  if %MAIN_ERRORS% EQU 0 (
    set PROJECT_ERRORS=0
  )  
) else (
  set PROJECT_ERRORS=1
)

call :OutputCompileStatus %PROJECT_ERRORS% "Project compilation"
REM ==============================================================================================================

:setESC
for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do rem"') do (
  set ESC=%%b
  exit /B 0
)

:OutputCompileStatus
if %~1 EQU 0 (
  echo %~2 %ESC%[32mfinished %ESC%[0m
  echo.
) else (
  echo %~2 %ESC%[31mfailed %ESC%[0m 
  echo.
)
exit /B 0